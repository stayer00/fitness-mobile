import React from 'react';
import { LogBox, SafeAreaView, StyleSheet } from 'react-native';

// import { NavigationContainer } from '@react-navigation/native';
import { AuthProvider } from '@store/Auth';
import { RootSiblingParent } from 'react-native-root-siblings';

import Main from '@components/Main';

import { mainContainer } from '@styles/app';

LogBox.ignoreLogs([
    'Out of bounds read',
]);

export default function App() {
    return (
        <SafeAreaView style={styles.container}>
            <AuthProvider>
                <RootSiblingParent>
                    {/* <NavigationContainer> */}
                        <Main />
                    {/* </NavigationContainer> */}
                </RootSiblingParent>
            </AuthProvider>
        </SafeAreaView >
    );
}

const styles = StyleSheet.create(mainContainer);