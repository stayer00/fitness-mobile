module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo', 'module:metro-react-native-babel-preset'],
    plugins: [
      [
        'module-resolver',
        {
          extensions: [
            '.ios.ts',
            '.android.ts',
            '.ts',
            '.ios.tsx',
            '.android.tsx',
            '.tsx',
            '.jsx',
            '.js',
            '.json',
          ],
          alias: {
            '@styles': "./src/styles",
            '@constants': "./src/constants",
            '@components': "./src/components",
            '@interfaces': "./src/interfaces",
            '@hooks': "./src/hooks",
            '@assets': "./src/assets",
            '@helpers': "./src/helpers",
            '@utils': "./src/utils",
            '@store': "./src/store"
          },
        },
      ],
      "react-native-reanimated/plugin"
    ],
  };
};
