module.exports = {
    resolver: {
        extraNodeModules: {
            src: `${__dirname}/src`,
            constants: `${__dirname}/src/constants`,
            components: `${__dirname}/src/components`,
            assets: `${__dirname}/src/assets`,
        },
    },
    // other configs
};