import React from 'react';

import { describe, expect, it, jest, xit } from '@jest/globals';
import { AuthProvider, AuthStore } from '@store/Auth';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import LoginScreen from '@components/AuthPage/LoginScreen';

import timer from '../timer';

Enzyme.configure({ adapter: new Adapter() });

describe('Auth tests', () => {
    it('Should correct handle login', async () => {
        AuthStore.prototype.isAuthenticated = jest.fn().mockImplementation(() => Promise.resolve());
        AuthStore.prototype.isAdmin = jest.fn().mockImplementation(() => Promise.resolve());
        AuthStore.prototype.login = jest.fn().mockResolvedValue(true);

        const mockOnLoginSuccess = jest.fn();

        const component = mount(
            <AuthProvider>
                <LoginScreen
                    onLoginSuccess={mockOnLoginSuccess}
                    onRegisterClick={() => jest.fn()}
                />
            </AuthProvider>,
        );

        await timer();
        component.update();
        
        component.find('Component[keyboardType="email-address"] TextInput')
            .simulate('change', { target: { value: 'email@mail.ru' } });

        component.find('Component[keyboardType="default"] TextInput')
            .simulate('change', { target: { value: 'Pass1' } });

        await timer();
        
        component.find('TouchableOpacity').simulate('click');
        await timer();

        expect(component).toMatchSnapshot();
        expect(mockOnLoginSuccess).toBeCalled();
    });
});