const timer = (timeout: number = 300) => new Promise<void>((resolve) => {
    setTimeout(() => {
        resolve();
    }, timeout);
});

export default timer;
