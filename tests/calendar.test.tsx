import React from 'react';

import useAddSeasonTickerHooks from '@hooks/useAddSeasonTickerHooks';
import { describe, expect, it, jest, xit } from '@jest/globals';
import { ClientProvider, ClientStore } from '@store/Clients';
import { TrainingAppointmentStore } from '@store/TrainingAppointment';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

import { defineDots, needRedDot } from '@components/Calendar/helper';

import { Service, TrainingAppointment } from '@interfaces/interfaces';

import * as requests from '@helpers/fetchFunction';

import { Colors } from '@constants/color';

import timer from './timer';
import { TrainingDay } from '@components/Calendar/TrainingDay';

describe('Calendar tests', () => {
    xit('Should add in object one date with red dot', () => {
        const dots = defineDots([{
            id: 1,
            service: 'service',
            serviceId: 1,
            clients: [],
            date: '2021-12-05T09:19:45.278Z',
            nameCoach: 'coact',
            coachId: 'id',
            loungeId: 1,
            nameLounge: 'lounge',
            isCanceled: false,
            currentClient: false,
        }]);

        expect(dots).toEqual({
            '2021-12-05': {
                disabled: false,
                dots: [
                    { key: 'service', color: Colors.Red, selectedDotColor: Colors.Red },
                ],
                selected: false,
            },
        });
    });

    xit('Should add green and red dots', () => {
        const dots = defineDots([{
            date: '2021-12-05T09:19:45.278Z',
            currentClient: false,
        } as TrainingAppointment, {
            date: '2021-12-05T09:19:45.278Z',
            currentClient: true,
        } as TrainingAppointment]);

        expect(dots['2021-12-05']?.dots[0]?.color).toEqual(Colors.Red);
        expect(dots['2021-12-05']?.dots[1]?.color).toEqual(Colors.Green);
    });
});

describe('Need add red dot tests', () => {
    xit('color is red, current client is false', () => {
        expect(needRedDot({
            '2021-12-05': {
                disabled: false,
                dots: [
                    { key: 'service', color: Colors.Red, selectedDotColor: Colors.Red },
                ],
                selected: false,
            },
        },
        '2021-12-05',
        {
            currentClient: false,
        } as TrainingAppointment,
        )).toBe(false);
    });

    xit('color is undefined, current client is true', () => {
        expect(needRedDot(
            {},
            '2021-12-05',
            {
                currentClient: true,
            } as TrainingAppointment,
        )).toBe(false);
    });

    xit('color is undefined, current client is false', () => {
        expect(needRedDot(
            {},
            '2021-12-05',
            {
                currentClient: false,
            } as TrainingAppointment,
        )).toBe(true);
    });
});

describe('Integration tests', () => {
    xit('Should be success notification after request', async () => {
        ClientStore.prototype.createStRequest = jest.fn().mockResolvedValue(200); // мокаем функцию, осуществляющую запрос на сервер

        let isCompleted = false;

        const TestComponent = () => {
            const {
                createNewSt,
                isSuccess,
                errortext,
            } = useAddSeasonTickerHooks();

            const func = async () => {
                await createNewSt(1, '1', '1', '1', {} as Service);
            };

            func();

            if (isCompleted) {
                setTimeout(() => {
                    expect(isSuccess).toBe(true);
                    expect(errortext).toBe('');
                }, 200);
            }
                
            return <></>;
        };

        const Component = () => (
            <ClientProvider>
                <TestComponent />
            </ClientProvider>
        );

        mount(<Component />);

        await timer(1000);
    });

    xit('Should be error notification after request', async () => {
        ClientStore.prototype.createStRequest = jest.fn().mockResolvedValue(400); // мокаем функцию, осуществляющую запрос на сервер

        let isCompleted = false;

        const TestComponent = () => {
            const {
                createNewSt,
                isSuccess,
                errortext,
            } = useAddSeasonTickerHooks();

            const func = async () => {
                await createNewSt(1, '1', '1', '1', {} as Service);
            };

            func();

            if (isCompleted) {
                setTimeout(() => {
                    expect(isSuccess).toBe(false);
                    expect(errortext).toBe('Error while buy season ticket');
                }, 200);
            }
                
            return <></>;
        };

        const Component = () => (
            <ClientProvider>
                <TestComponent />
            </ClientProvider>
        );

        mount(<Component />);

        await timer(1000);
    });
});

describe('loadClientTrainingAppointments', () => {
    xit('trainingAppointments = undefined', async () => {
        jest.spyOn(requests, 'fetchFunctionApi').mockResolvedValue(undefined);

        const store = new TrainingAppointmentStore('id');
        await store.loadClientTrainingAppointments('id');

        expect(store.state).toEqual('error');
    });

    xit('trainingAppointments = true', async () => {
        jest.spyOn(requests, 'fetchFunctionApi').mockResolvedValue(true);

        const store = new TrainingAppointmentStore('id');
        await store.loadClientTrainingAppointments('id');

        expect(store.state).toEqual('error');
    });    

    xit('trainingAppointments = []', async () => {
        jest.spyOn(requests, 'fetchFunctionApi').mockResolvedValue([]);

        const store = new TrainingAppointmentStore('id');
        await store.loadClientTrainingAppointments('id');

        expect(store.state).toEqual('loaded');
    });    
});

describe('loadClientTrainingAppointments - test cycle', () => {
    it('test load training appointments', async () => {
        jest.spyOn(requests, 'fetchFunctionApi').mockResolvedValue([{
            clients: [],
        }]);

        const store = new TrainingAppointmentStore('id');
        await store.loadClientTrainingAppointments('id');

        expect(store.trainingAppointments.length > 0).toEqual(true);
        expect(store.trainingAppointments[0].currentClient).toEqual(false);
    });

    xit('test training appointment', async () => {
        jest.spyOn(requests, 'fetchFunctionApi').mockResolvedValue([{
            clients: [],
        }, {
            clients: [{ clientId: 'id' }],
        }]);

        const store = new TrainingAppointmentStore('id');
        await store.loadClientTrainingAppointments('id');

        expect(store.trainingAppointments[0].currentClient).toEqual(false);
        expect(store.trainingAppointments[1].currentClient).toEqual(true);
    });
});

describe('Modules', () => {
    const headers = new Headers();
    headers.append('content-type', 'application/json');

    global.fetch = jest.fn(() =>
        Promise.resolve({
            json: () => Promise.resolve([{
                id: 'id',
                clients: [],
            }] as any),
            status: 200,
            ok: true,
            headers,
        } as Response),
    );

    xit('module 2 is not ready', async () => {
        jest.spyOn(requests, 'fetchFunctionApi').mockResolvedValue([{
            id: 'id',
            clients: [],
        }]);

        const store = new TrainingAppointmentStore('id');
        await store.loadClientTrainingAppointments('id');

        expect(store.trainingAppointments.length).toEqual(1);
    });

    xit('all modules are ready', async () => {
        const store = new TrainingAppointmentStore('id');
        await store.loadClientTrainingAppointments('id');

        expect(store.trainingAppointments.length).toEqual(1);
    });
});

describe('Regression testing training day', () => {
    xit('Correct rendering training day and check call handlers', () => {
        const mockSetClient = jest.fn();
        const mockOnUnsetClient = jest.fn();
        const mockOnBackAction = jest.fn();
    
        const component = mount(
            <TrainingDay
                trainingAppointments={[{
                    service: 'service1',
                    nameCoach: 'nameCoach1',
                    date: '2021-12-05T10:00:00.000Z',
                    currentClient: false,
                } as TrainingAppointment, {
                    service: 'service2',
                    nameCoach: 'nameCoach2',
                    date: '2021-12-06T10:00:00.000Z',
                    currentClient: true,
                } as TrainingAppointment]}
                onSetClient={mockSetClient}
                onUnsetClient={mockOnUnsetClient}
                onBackAction={mockOnBackAction}
            />,
        );
    
        expect(component).toMatchSnapshot();
    
        component.find('Circle[color="#c60408"] ForwardRef').simulate('click');
        component.find('Circle[color="#73c673"] ForwardRef').simulate('click');
    
        expect(mockSetClient).toBeCalledTimes(1);
        expect(mockOnUnsetClient).toBeCalledTimes(1);
    });
});
