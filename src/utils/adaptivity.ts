import { Dimensions, PixelRatio } from 'react-native';

const { height, width } = Dimensions.get('window');

export const getHeight = (h: number) =>
    Math.round(PixelRatio.roundToNearestPixel((height / 812) * h));

export const getWidth = (w: number) =>
    Math.round(PixelRatio.roundToNearestPixel((width / 375) * w));

export const model = width / height < 0.5;

export const font = (f: number) =>
    Math.round(
        PixelRatio.roundToNearestPixel(Math.min(width / 375, height / 812) * f),
    );

// export const TAB_BAR_HEIGHT = 15 + 17 + 3 + font(11) + getWidth(26) + initialWindowMetrics.insets.bottom;