import { useLocale } from '@hooks/useLocale';

export const SidebarItem = {
    FitnessClub: 'Fitness club',
    Calendar:  useLocale({ 'en': 'Calendar', 'ru': 'Календарь' }),
    Services: useLocale({ 'en': 'Services', 'ru': 'Услуги' }),
    LogOut: useLocale({ 'en': 'LogOut', 'ru': 'Выйти' }),
    Register: useLocale({ 'en': 'Register', 'ru': 'Зарегистрироваться' }),
    Login: useLocale({ 'en': 'Login', 'ru': 'Выйти' }),
};
