export enum Colors {
    Orange = '#EF9021',
    DisabledOrange = '#ef902145',
    LightOrange = '#FFB762',
    Black = 'black',
    Green = '#73c673',
    Red = '#c60408',
    White = '#fff',
    GrayDark = 'rgba(104, 104, 104, 1)',
}

export enum PositionCircle {
    RightBottom = 'right-bottom',
    LeftBottom = 'left-bottom',
    Center = 'center',
}