// Tab ICons...
export const sidebarImages = new Map<string, string>([
    ['home', 'home.png'],
    ['search', 'search.png'],
    ['notifications', 'bell.png'],
    ['settings', 'settings.png'],
    ['logout', 'logout.png'],
]);
