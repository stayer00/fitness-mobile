import { StyleProp, TextStyle, ViewStyle } from 'react-native';

import { Colors } from '@constants/color';

export const opacitySidebarTitle: StyleProp<TextStyle> = {
    marginTop: 6,
    color: Colors.Orange,
    borderColor: Colors.Orange,
    borderWidth: 1,
    margin: 25,
    padding: 5,
    borderRadius: 5,
    textAlign: 'center',
};

export const sidebarContainer = { flexGrow: 1, marginTop: 50 };

export const tabBtnContainer: StyleProp<ViewStyle> = {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    backgroundColor: 'transparent',
    paddingLeft: 13,
    paddingRight: 35,
    borderRadius: 0,
    marginTop: 15,
};

export const getTabBtnTextStyles = (currentTab: string, title: string): StyleProp<TextStyle> => ({
    fontSize: 15,
    fontWeight: 'bold',
    paddingLeft: 15,
    color: currentTab === title ? Colors.Orange : Colors.LightOrange,
    borderLeftColor: currentTab === title ? Colors.Orange : Colors.Black,
    borderLeftWidth: currentTab === title ? 1 : 0,
});

export const getIconStyles = (currentTab: string, title: string) => ({
    width: 20,
    height: 20,
    tintColor: currentTab == title ? Colors.Orange : Colors.Black,
    marginRight: 10,
});
