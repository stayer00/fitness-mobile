import { Animated } from 'react-native';

import { Colors } from '@constants/color';

export const getAnimateSidebar = (showMenu: boolean, scaleValue: Animated.Value, offsetValue: Animated.Value): Animated.Value => ({
    flexGrow: 1,
    border: `1px solid ${Colors.Orange}`,
    backgroundColor: 'black',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: showMenu ? 15 : 0,
    // Transforming View...
    transform: [
        { scale: scaleValue },
        { translateX: offsetValue },
    ],
});
