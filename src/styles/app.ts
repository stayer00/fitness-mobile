import { StyleSheet, Animated } from 'react-native';

import { Colors } from '@constants/color';

export const mainContainer: StyleSheet.NamedStyles<any> = {
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        width: '100%',
    },
};

export const getCloseButtonOffsetAmimation = (showMenu: boolean): Animated.TimingAnimationConfig => ({
    // YOur Random Value...
    toValue: !showMenu ? 0 : 0,
    duration: 300,
    useNativeDriver: true,
    flexDirection: 'row',
});

export const headerStyles: StyleSheet.NamedStyles<any> = {
    fontSize: 30,
    fontWeight: 'bold',
    color: 'black',
    width: '100%',
    textAlign: 'center',
    position: 'absolute',
    transform: [{
        translateY: 10,
    }],
};

export const openCloseMenuIcon: StyleSheet.NamedStyles<any> = {
    width: 20,
    height: 20,
    tintColor: 'black',
    marginLeft: 20,
    transform: [{
        translateY: 20,
    }],
};

export const headerContainerStyle: StyleSheet.NamedStyles<any> = {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    position: 'relative',
    height: 60,
};

export const getStyleAnimateHeader = (closeButtonOffset: Animated.Value): StyleSheet.NamedStyles<any> => ({
    transform: [{
        translateY: closeButtonOffset,
    }],
    justifyContent: 'center',
    backgroundColor: Colors.Orange,
    borderBottomWidth: 1,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
});

