import React, { FC, useState } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';

import { useLocale } from '@hooks/useLocale';
import { AuthContext } from '@store/Auth';
import { ClientContext } from '@store/Clients';
import { CreditCardInput } from 'react-native-credit-card-input';

import Overlay from '@components/Overlay';

import { getHeight, getWidth } from '@utils/adaptivity';

import { useStore } from '@helpers/useStore';

import { Colors } from '@constants/color';

type CachInProps = {
    visible: boolean;
    onClose: () => void;
};

export const CachIn: FC<CachInProps> = ({ visible, onClose }) => {

    const storeAuth = useStore(AuthContext);
    const { pay, client } = useStore(ClientContext);

    const [cvc, setCvc] = useState<string>('');
    const [expiry, setExpiry] = useState<string>('');
    const [number, setNumber] = useState<string>('');
    const [balance, setBalance] = useState<number>(0);

    const handleInputChange = ({ values }: any) => {
        const {
            cvc,
            expiry,
            number,
        } = values;
        
        setNumber(number);
        setCvc(cvc);
        setExpiry(expiry);
    };

    const onPay = () => {
        pay({
            cvc,
            expiry,
            name: '',
            number,
            balance: Number(balance),
        }).then((answ) => {
            if (answ) {
                if (client) {
                    client.balance = (client.balance ?? 0) + balance;
                }
            }
            onClose();
        });
    };

    return (
        <Overlay
            style={{
                justifyContent: 'center',
                backgroundColor: Colors.White,
            }}
            closeCallback={onClose}
            visible={visible}
        >
            <View
                style={{
                    display: 'flex',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    // padding: getHeight(50),
                }}
            >
                <CreditCardInput onChange={handleInputChange} />

                <View>
                    <TextInput
                        style={{
                            fontWeight: 'bold',
                            borderWidth: 1,
                            borderColor: Colors.Black,
                            paddingHorizontal: getWidth(25),
                            paddingVertical: getHeight(10),
                            borderRadius: getWidth(5),
                            textAlign: 'center',
                            marginBottom: getHeight(35),
                        }}
                        value={balance.toString()}
                        keyboardType="numeric"
                        onChangeText={(text) => setBalance(Number(text))}
                    />
                    <TouchableOpacity
                        onPress={onPay}
                    >
                        <Text
                            style={{
                                fontWeight: 'bold',
                                borderWidth: 1,
                                textAlign: 'center',
                                borderColor: Colors.Black,
                                paddingHorizontal: getWidth(25),
                                paddingVertical: getHeight(10),
                                borderRadius: getWidth(5),
                            }}
                        >
                            {
                                useLocale({ 'en': 'Pay', 'ru': 'Пополнить баланс' })
                            }
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Overlay>
    );
};
