import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { useLocale } from '@hooks/useLocale';
import { ClientContext } from '@store/Clients';
import { observer } from 'mobx-react';

import { CachIn } from '@components/CachIn/CachIn';

import { getHeight } from '@utils/adaptivity';

import { useStore } from '@helpers/useStore';

import { SidebarItem } from '@constants/enum';

import { opacitySidebarTitle, sidebarContainer } from '../../styles/sidebar';
import { TabButton } from './TabBtn';

// For multiple Buttons...

interface SidebarMenuProps {
    currentTab: string;
    onUpdateCurrentTab: (tab: SidebarItem) => void;
}

export const SidebarMenu = observer(
    ({ currentTab, onUpdateCurrentTab: setCurrentTab }: SidebarMenuProps) => {
        const { client } = useStore(ClientContext);

        const [balanceVisible, setbalanceVisible] = useState(false);
        return (
            <>
                <CachIn
                    visible={balanceVisible}
                    onClose={() => setbalanceVisible(false)}
                />

                <View
                    style={{
                        justifyContent: 'flex-start',
                        padding: 15,
                        marginTop: 20,
                    }}
                >
                    <TouchableOpacity>
                        <Text style={opacitySidebarTitle}>Fitness club</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ marginTop: getHeight(20) }}
                        onPress={() => setbalanceVisible(true)}
                    >
                        <Text
                            style={opacitySidebarTitle}
                        >{`${useLocale({
                                'en': 'Balance',
                                'ru': 'Баланс',
                            })}: ${client?.balance ?? 0}`}</Text>
                    </TouchableOpacity>

                    <View style={sidebarContainer}>
                        {TabButton(currentTab, setCurrentTab, SidebarItem.FitnessClub)}
                        {TabButton(currentTab, setCurrentTab, SidebarItem.Calendar)}
                        {TabButton(currentTab, setCurrentTab, SidebarItem.Services)}
                    </View>

                    <View>
                        {TabButton(currentTab, setCurrentTab, SidebarItem.LogOut)}
                    </View>
                </View>
            </>
        );
    },
);
