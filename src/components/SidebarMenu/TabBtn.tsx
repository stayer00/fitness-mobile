import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';

import { SidebarItem } from '@constants/enum';

import { getIconStyles, getTabBtnTextStyles, tabBtnContainer } from '@styles/sidebar';

export const TabButton = (
    currentTab: string,
    onUpdateCurrentTab: (tab: SidebarItem) => void,
    title: SidebarItem,
) => {
    let icon = require('../../assets/loading.png');
    switch (title) {
        case SidebarItem.FitnessClub:
            icon = require('../../assets/home.png');
            break;
        case SidebarItem.Calendar:
            icon = require('../../assets/calendar.png');
            break;
        case SidebarItem.Services:
            icon = require('../../assets/settings.png');
            break;
        case SidebarItem.LogOut:
            icon = require('../../assets/logout.png');
            break;
        default:
            break;
    }

    return (
        <TouchableOpacity onPress={() => {
            onUpdateCurrentTab(title);
        }}>
            <View style={tabBtnContainer}>
                <Image
                    source={icon}
                    style={getIconStyles(currentTab, title)}
                />

                <Text style={getTabBtnTextStyles(currentTab, title)}>{title}</Text>
            </View>
        </TouchableOpacity>
    );
};
