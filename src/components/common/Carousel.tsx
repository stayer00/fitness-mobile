import React, { FC, memo, useEffect, useMemo, useRef } from 'react';
import {
    StyleSheet,
    Image,
    StyleProp,
    ViewStyle,
    View,
    Text,
} from 'react-native';

import Carousel, { Pagination } from 'react-native-snap-carousel';

import { Colors } from '@constants/color';

import { getHeight } from '../../utils/adaptivity';

interface CarouselComponentProp {
    images: Array<{ id: string; image: any }>;
    width: number;
    indexSelected: number;
    paginationStyles?: StyleProp<ViewStyle>;
    onSwipe: (slideIndex: number) => void;
}

const CarouselComponent: FC<CarouselComponentProp> = ({
    images,
    width,
    indexSelected,
    paginationStyles = {},
    onSwipe,
}) => {
    let ref: Carousel<{
        id: string;
        image: any;
    }> | null = null;

    useEffect(() => {
        if (ref) {
            ref?.snapToItem(indexSelected);
        }
    }, [indexSelected]);
    return (
        <>
            <Carousel
                // ref={ref}
                ref={(_carouselRef) => {
                    ref = _carouselRef;
                }}
                layout={'stack'}
                style={{ display: 'flex' }}
                data={images}
                initialNumToRender={3}
                contentContainerCustomStyle={{
                    display: 'flex',
                    flexGrow: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: getHeight(50),
                }}
                sliderWidth={width}
                itemWidth={width}
                renderItem={({ item, index }) => {
                    return (
                        <Image
                            key={index}
                            style={styles.image}
                            resizeMode="contain"
                            source={{ uri: item.image }}
                        />
                    );
                }}
                onSnapToItem={onSwipe}
            />

            <Pagination
                containerStyle={paginationStyles}
                inactiveDotColor="gray"
                dotColor={'orange'}
                activeDotIndex={indexSelected}
                dotsLength={images.length}
                animatedDuration={150}
                inactiveDotScale={1}
                renderDots={(activeIndex) =>
                    images.map((_, i) => (
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                            key={i}
                            onTouchStart={() => onSwipe(i)}
                        >
                            <Text
                                style={{
                                    color: i === activeIndex ? Colors.Orange : '#000',
                                    fontSize: 30,
                                }}
                            >
                  *
                            </Text>
                        </View>
                    ))
                }
            />
        </>
    );
};

export default memo(
    CarouselComponent, ({
        images: prevImages,
        indexSelected: prevIndexSelected,
    }, {
        images: currImages,
        indexSelected: currIndexSelected,
    }) => {
        const areEqual = (
            currImages.length === prevImages.length
        && prevIndexSelected === currIndexSelected
        );

        // console.log({
        //     areEqual,
        //     currIndexSelected,
        // });
        
        return areEqual;
    });

const styles = StyleSheet.create({
    image: {
        width: '100%', height: getHeight(200),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        // elevation: 5,
    },
});
