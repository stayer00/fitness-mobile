import React from 'react';
import { StyleSheet, TouchableOpacity, StyleProp, ViewStyle } from 'react-native';

import { Colors, PositionCircle } from '@constants/color';

interface CircleProp {
    position?: PositionCircle,
    size?: number,
    rotation?: number,
    children: React.ReactNode;
    color?: Colors;
    onClick?: () => void;
}

export const Circle = ({ children, position, size = 55, rotation = 0, color, onClick }: CircleProp) => {
    const getStyle = (): StyleProp<ViewStyle> => {
        const sizeCircle: StyleProp<ViewStyle> = { width: size, height: size };
        const rotationCircle: StyleProp<ViewStyle> = { rotation: rotation };
        if (position === PositionCircle.Center) {
            return {
                ...styles.container,
                ...sizeCircle,
                ...rotationCircle,
                backgroundColor: color,
            };
        }
        return {
            ...styles.container,
            ...sizeCircle,
            ...rotationCircle,
            position: 'absolute',
            bottom: 0,
            right: 0,
        };
    };
    return (
        <TouchableOpacity
            activeOpacity={0.95}
            onPress={onClick}
            style={getStyle()}
        >
            {children}
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        borderRadius: 55,
        backgroundColor: Colors.Orange,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
