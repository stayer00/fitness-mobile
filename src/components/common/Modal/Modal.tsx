import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    ViewStyle,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import Animated, {
    useAnimatedStyle,
    useSharedValue,
    withTiming,
} from 'react-native-reanimated';

import { getHeight, getWidth } from '@utils/adaptivity';

import { Colors } from '@constants/color';

import Overlay from '../../Overlay';

interface FormModalProps {
    style?: ViewStyle,
    visible: boolean,
    closeCallback: () => void,
    slideTime?: number,
    title: string,
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        minHeight: 52,
        marginTop: 6,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
});

const FormModal: React.FC<FormModalProps> = ({
    style = {},
    visible,
    closeCallback,
    slideTime = 200,
    title,
    children,
}) => {
    
    const [contentHeight, setContentHeight] = useState(0);
    const offsetY = useSharedValue(1);

    useEffect(() => {
        offsetY.value = visible ? 0 : 1;
    }, [visible]);

    const animatedStyles = useAnimatedStyle(() => ({
        transform: [
            {
                translateY: withTiming(offsetY.value * contentHeight, { duration: slideTime }),
            },
        ],
    }),
    );

    const containerStyles: ViewStyle[] = [
        styles.container,
        animatedStyles,
        style,
    ];

    return (
        <Overlay visible={visible} closeCallback={closeCallback}>
            <Animated.View
                style={containerStyles}
                onLayout={(e) => setContentHeight(e.nativeEvent.layout.height)}
            >
                <View
                    style={{
                        position: 'relative',
                        width: '100%',
                        height: getHeight(52),
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: Colors.Black,
                    }}
                >
                    <Text
                        style={{
                            color: Colors.Black,
                        }}
                    >
                        {title}
                    </Text>

                    <TouchableOpacity
                        onPress={closeCallback}
                        style={{
                            position: 'absolute',
                            right: getWidth(14),
                        }}
                    >
                        {/* В данном контексте не clear а close */}
                        {/* <CloseSvg fill={Colors.Orange}/> */}
                    </TouchableOpacity>
                </View>

                <View
                    style={{
                        flexDirection: 'column',
                    }}
                >
                    {children}
                </View>
            </Animated.View>
        </Overlay>
    );
};

export default FormModal;

