// import React, { FC } from 'react';
// import { StyleSheet } from 'react-native';
import React, { FC, useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { Picker } from '@react-native-picker/picker';

import Overlay from '@components/Overlay';

import { Colors } from '@constants/color';

type DropdonwProps = {
    selectedValue: string | number;
    values: Array<{ title: string; key: string | number }>;
    title: string;
    onSelectedNewValue: (newSelectedValue: string | number) => void;
};

export const Dropdown: FC<DropdonwProps> = ({
    title,
    selectedValue,
    values,
    onSelectedNewValue,
}: DropdonwProps) => {
    const [visible, setVisible] = useState(false);

    if (visible) {
        console.log('selectedValue', selectedValue);
        
        return (
            <Overlay
                style={{
                    justifyContent: 'center',
                    backgroundColor: Colors.White,
                }}
                closeCallback={() => setVisible(false)}
                visible={visible}
            >
                <View style={styles.container}>
                    <Picker
                        selectedValue={selectedValue}
                        style={{ width: 300, display: 'flex', alignContent: 'center' }}
                        onValueChange={(itemValue, itemIndex) => onSelectedNewValue(itemValue)}
                    >
                        {values.map(({ title: handledTitle, key }) => (
                            <Picker.Item label={handledTitle} value={key} />
                        ))}
                    </Picker>
                </View>
            </Overlay>
        );
    }

    return (
        <View
            onTouchStart={() => setVisible(true)}
            style={{ width: '100%' }}
        >
            <Text
                style={styles.text}
            >
                {title}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    text: {
        textAlign: 'center',
        fontSize: 24,
    },
});
