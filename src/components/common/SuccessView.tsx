import React, { FC, ReactNode } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';

import Overlay from '@components/Overlay';

import { Colors } from '@constants/color';

type SuccessViewType = {
    text: string;
    visible: boolean;
    children?: ReactNode;
    onClose: () => void;
};

export const SuccessView: FC<SuccessViewType> = ({
    text,
    visible,
    onClose,
}) => (
    <Overlay
        closeCallback={onClose}
        visible={visible}
    >
        <View
            style={{
                flex: 1,
                backgroundColor: Colors.Black,
                justifyContent: 'center',
            }}
        >
            <Image
                source={require('../../assets/success.png')}
                style={{
                    height: 150,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                }}
            />
            <Text style={styles.successTextStyle}>{`${text}`}</Text>
            <TouchableOpacity
                style={[
                    styles.buttonStyle,
                    { backgroundColor: Colors.Green, borderWidth: 0 },
                ]}
                activeOpacity={0.5}
                onPress={onClose}
            >
                <Text style={styles.buttonTextStyle}>Ok</Text>
            </TouchableOpacity>
        </View>
    </Overlay>
);

const styles = StyleSheet.create({
    buttonStyle: {
        color: '#FFFFFF',
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: Colors.Orange,
        borderWidth: 1,
        borderColor: Colors.LightOrange,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    },
});
