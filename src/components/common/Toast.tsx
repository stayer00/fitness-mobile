import React, { FC } from 'react';

import ToastRN from 'react-native-root-toast';

import { Colors } from '@constants/color';

type ToastProps = {
    visible: boolean;
    message: string;
};

const Toast: FC<ToastProps> = ({ visible, message }) => {
    return (
        <ToastRN
            backgroundColor={Colors.Orange}
            textColor="#000"
            duration={5000}
            visible={visible}
        >
            {message}
        </ToastRN>
    );
};

export default Toast;
