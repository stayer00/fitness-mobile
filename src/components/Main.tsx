import React, { useEffect, useRef, useState } from 'react';
import {
    Animated,
    Image,
    PanResponderGestureState,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import useNotification from '@hooks/useNotification';
import { AuthContext, AuthType, TypeUser } from '@store/Auth';
import { ClientProvider } from '@store/Clients';
import { CoachesProvider } from '@store/Coaches';
import { ServicesProvider } from '@store/Services';
import { TrainingAppointmentProvider } from '@store/TrainingAppointment';
import { observer } from 'mobx-react';

import LoginScreen from '@components/AuthPage/LoginScreen';
import RegisterScreen from '@components/AuthPage/RegisterScreen';
import { CalendarView } from '@components/Calendar/CalendarView';
import { SeasonTicketView } from '@components/SeasonTicketView/SeasonTicketView';
import { ServicesView } from '@components/Services/ServicesView';
import { SidebarMenu } from '@components/SidebarMenu/SidebarMenu';

import { useStore } from '@helpers/useStore';

import { SidebarItem } from '@constants/enum';

import { getAnimateSidebar } from '@styles/animation';
import {
    getCloseButtonOffsetAmimation,
    getStyleAnimateHeader,
    headerContainerStyle,
    headerStyles,
    mainContainer,
    openCloseMenuIcon,
} from '@styles/app';

export default observer(() => {
    const { userId, auth } = useStore(AuthContext);
    useNotification();

    const [currentTab, setCurrentTab] = useState<SidebarItem>(
        SidebarItem.Register,
    );
    // To get the curretn Status of menu ...
    const [showMenu, setShowMenu] = useState(false);

    // Animated Properties...

    const offsetValue = useRef(new Animated.Value(0)).current;
    // Scale Intially must be One...
    const scaleValue = useRef(new Animated.Value(1)).current;
    const closeButtonOffset = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        if (auth === AuthType.Unauthorized) {
            setCurrentTab(SidebarItem.Login);
        } else {
            setCurrentTab(SidebarItem.FitnessClub);
        }
    }, [auth]);

    const onMenuSwipe = (
        gestureName?: string,
        gestureState?: PanResponderGestureState,
    ) => {
    // const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
    // if (gestureName && gestureState) {
    //     if (gestureName === SWIPE_LEFT && !showMenu) {
    //         return;
    //     }
    //     if (gestureName === SWIPE_RIGHT && showMenu) {
    //         return;
    //     }
    //     if (gestureName === SWIPE_UP || gestureName === SWIPE_DOWN) {
    //         return;
    //     }
    // }
    // Do Actions Here....
    // Scaling the view...
        Animated.timing(scaleValue, {
            toValue: showMenu ? 1 : 0.95,
            duration: 300,
            useNativeDriver: true,
        }).start();

        Animated.timing(offsetValue, {
            // YOur Random Value...
            toValue: showMenu ? 0 : 180,
            duration: 300,
            useNativeDriver: true,
        }).start();

        Animated.timing(
            closeButtonOffset,
            getCloseButtonOffsetAmimation(showMenu),
        ).start();

        setShowMenu(!showMenu);
    };

    const getMainView = () => {
        switch (currentTab) {
            case SidebarItem.FitnessClub:
                return <SeasonTicketView />;
            case SidebarItem.Calendar:
                return (
                    <TrainingAppointmentProvider
                        id={userId}>
                        <CalendarView />
                    </TrainingAppointmentProvider>
                );
            case SidebarItem.Services:
                return <ServicesView />;
            case SidebarItem.LogOut:
            case SidebarItem.Login:
                return (
                    <LoginScreen
                        onLoginSuccess={() => setCurrentTab(SidebarItem.FitnessClub)}
                        onRegisterClick={() => setCurrentTab(SidebarItem.Register)}
                    />
                );
            case SidebarItem.Register:
                return (
                    <RegisterScreen
                        onLoginClick={() => setCurrentTab(SidebarItem.Login)}
                    />
                );
            default:
                return <View />;
        }
    };

    const isHeaderVisible = () => {
        const tabs = [SidebarItem.LogOut, SidebarItem.Register, SidebarItem.Login];

        if (tabs.includes(currentTab)) {
            return false;
        }
        return true;
    };

    return (
        <SafeAreaView style={mainContainer.container}>
            <ClientProvider>
                <SidebarMenu
                    currentTab={currentTab}
                    onUpdateCurrentTab={(tab: SidebarItem) => {
                        onMenuSwipe();
                        setCurrentTab(tab);
                    }}
                />

                <Animated.View
                    style={getAnimateSidebar(showMenu, scaleValue, offsetValue)}
                >
                    <Animated.View style={getStyleAnimateHeader(closeButtonOffset)}>
                        {isHeaderVisible() && (
                            <TouchableOpacity
                                style={headerContainerStyle}
                                onPress={() => {
                                    onMenuSwipe();
                                }}
                            >
                                <Image
                                    source={
                                        showMenu
                                            ? require('../assets/close.png')
                                            : require('../assets/menu.png')
                                    }
                                    style={openCloseMenuIcon}
                                />

                                <Text style={headerStyles}>{currentTab}</Text>
                            </TouchableOpacity>
                        )}
                    </Animated.View>

                    <View
                        // onSwipe={onMenuSwipe}
                        style={{ backgroundColor: 'transparent', flexGrow: 1 }}
                    >
                        <CoachesProvider>
                            <ServicesProvider>
                                {getMainView()}
                            </ServicesProvider>
                        </CoachesProvider>
                    </View>
                </Animated.View>
            </ClientProvider>
        </SafeAreaView>
    );
});

const styles = StyleSheet.create(mainContainer);
