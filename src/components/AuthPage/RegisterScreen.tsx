import React, { useState, createRef, FC } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    Image,
    KeyboardAvoidingView,
    Keyboard,
    TouchableOpacity,
    ScrollView,
    Platform,
} from 'react-native';

import { useLocale } from '@hooks/useLocale';
import DateTimePicker from '@react-native-community/datetimepicker';
import { AuthContext } from '@store/Auth';

import { getHeight } from '@utils/adaptivity';

import { useStore } from '@helpers/useStore';

import { Colors } from '@constants/color';

import Loader from '../common/Loader';

type RegisterScreenProps = {
    onLoginClick: () => void;
};

const RegisterScreen: FC<RegisterScreenProps> = ({ onLoginClick }) => {
    const { register } = useStore(AuthContext);

    const [userName, setUserName] = useState('');
    const [userSurname, setUserSurname] = useState('');
    const [userEmail, setUserEmail] = useState('');
    const [userGender, setUserGender] = useState('');
    const [phone, setUserPhone] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [errortext, setErrortext] = useState('');

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const [isRegistraionSuccess, setIsRegistraionSuccess] = useState(false);

    const emailInputRef = createRef<any>();
    const ageInputRef = createRef<any>();
    const phoneInputRef = createRef<any>();
    const passwordInputRef = createRef<any>();
    // fetchServices();

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const handleSubmitButton = async () => {
        setErrortext('');
        if (!userName) {
            alert(useLocale({
                'en': 'Please fill Name',
                'ru': 'Пожалуйста, введите имя',
            }));
            return;
        }
        if (!userSurname) {
            alert(useLocale({
                'en': 'Please fill Surname',
                'ru': 'Пожалуйста, введите фамилию',
            }));
            return;
        }
        if (!userEmail) {
            alert(useLocale({
                'en': 'Please fill Email',
                'ru': 'Пожалуйста, введите email',
            }));
            return;
        }
        if (!userGender) {
            alert(useLocale({
                'en': 'Please fill Gender',
                'ru': 'Пожалуйста, введите пол',
            }));
            return;
        }
        if (!phone) {
            alert(useLocale({
                'en': 'Please fill Phone',
                'ru': 'Пожалуйста, введите номер телефона',
            }));
            return;
        }
        if (!userPassword) {
            alert(useLocale({
                'en': 'Please fill Password',
                'ru': 'Пожалуйста, введите пароль',
            }));
            return;
        }
        if (!confirmPassword) {
            alert(useLocale({
                'en': 'Please fill Confirm Password',
                'ru': 'Пожалуйста, введите подтверждение пароля',
            }));
            return;
        }
        //Show Loader
        setLoading(true);

        const registerResult = await register({
            name: userName,
            password: userPassword,
            surname: userSurname,
            passwordConfirm: confirmPassword,
            numberPhone: phone,
            email: userEmail,
            gender: userGender,
            birthday: date.toISOString(),
        }, false);

        setLoading(false);
        if (registerResult) {
            setIsRegistraionSuccess(true);
        } else {
            setErrortext(useLocale({
                'en': 'Register failed',
                'ru': 'Ошибка регистрации',
            }));
        }
    };
    
    if (isRegistraionSuccess) {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: Colors.Black,
                    justifyContent: 'center',
                }}
            >
                <Image
                    source={require('../../assets/success.png')}
                    style={{
                        height: 150,
                        resizeMode: 'contain',
                        alignSelf: 'center',
                    }}
                />

                <Text style={styles.successTextStyle}>
                    {useLocale({
                        'en': 'Registration Successful',
                        'ru': 'Успешная регистрация',
                    })}
                </Text>

                <TouchableOpacity
                    style={[
                        styles.buttonStyle,
                        { backgroundColor: Colors.Green, borderWidth: 0 },
                    ]}
                    activeOpacity={0.5}
                    onPress={onLoginClick}
                >
                    <Text style={styles.buttonTextStyle}>
                        {useLocale({
                            'en': 'Login Now',
                            'ru': 'Войти',
                        })}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: Colors.Black }}>
                <Loader loading={loading} />
                <ScrollView
                    keyboardShouldPersistTaps="handled"
                    contentContainerStyle={{
                        justifyContent: 'center',
                        alignContent: 'center',
                    }}
                >
                    <View style={{ alignItems: 'center' }}>
                        <Image
                            source={require('../../assets/spinning.png')}
                            style={{
                                width: '50%',
                                height: getHeight(100),
                                resizeMode: 'contain',
                                margin: 30,
                            }}
                        />
                    </View>
                    <KeyboardAvoidingView enabled>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserName) => setUserName(UserName)}
                                underlineColorAndroid="#f000"
                                placeholder={useLocale({
                                    'en': 'Enter Name',
                                    'ru': 'Введите имя',
                                })}
                                placeholderTextColor="#8b9cb5"
                                autoCapitalize="sentences"
                                returnKeyType="next"
                                onSubmitEditing={() => emailInputRef.current?.focus()}
                                blurOnSubmit={false}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserSurname) => setUserSurname(UserSurname)}
                                underlineColorAndroid="#f000"
                                placeholder={useLocale({
                                    'en': 'Enter Surname',
                                    'ru': 'Введите фамилию',
                                })}
                                placeholderTextColor="#8b9cb5"
                                autoCapitalize="sentences"
                                returnKeyType="next"
                                onSubmitEditing={() => emailInputRef.current?.focus()}
                                blurOnSubmit={false}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserEmail) => setUserEmail(UserEmail)}
                                underlineColorAndroid="#f000"
                                placeholder={useLocale({
                                    'en': 'Enter Email',
                                    'ru': 'Введите почту',
                                })}
                                placeholderTextColor="#8b9cb5"
                                keyboardType="email-address"
                                ref={emailInputRef}
                                returnKeyType="next"
                                onSubmitEditing={() => passwordInputRef?.current?.focus()}
                                blurOnSubmit={false}
                            />
                        </View>

                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={setUserPhone}
                                underlineColorAndroid="#f000"
                                placeholder={useLocale({
                                    'en': 'Enter Phone',
                                    'ru': 'Введите номер телефона',
                                })}
                                placeholderTextColor="#8b9cb5"
                                autoCapitalize="sentences"
                                keyboardType="numeric"
                                ref={phoneInputRef}
                                returnKeyType="next"
                                onSubmitEditing={Keyboard.dismiss}
                                blurOnSubmit={false}
                            />
                        </View>

                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserAge) => setUserGender(UserAge)}
                                underlineColorAndroid="#f000"
                                placeholder={useLocale({
                                    'en': 'Enter Gender',
                                    'ru': 'Введите пол',
                                })}
                                placeholderTextColor="#8b9cb5"
                                ref={ageInputRef}
                                returnKeyType="next"
                                onSubmitEditing={() => phoneInputRef.current?.focus()}
                                blurOnSubmit={false}
                            />
                        </View>

                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserPassword) => setUserPassword(UserPassword)}
                                underlineColorAndroid="#f000"
                                placeholder={useLocale({
                                    'en': 'Enter Password',
                                    'ru': 'Введите пароль',
                                })}
                                placeholderTextColor="#8b9cb5"
                                ref={passwordInputRef}
                                returnKeyType="next"
                                secureTextEntry={true}
                                onSubmitEditing={() => ageInputRef.current?.focus()}
                                blurOnSubmit={false}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={setConfirmPassword}
                                underlineColorAndroid="#f000"
                                placeholder={useLocale({
                                    'en': 'Confirm Password',
                                    'ru': 'Подтвердите пароль',
                                })}
                                placeholderTextColor="#8b9cb5"
                                ref={passwordInputRef}
                                returnKeyType="next"
                                secureTextEntry={true}
                                onSubmitEditing={() => ageInputRef.current?.focus()}
                                blurOnSubmit={false}
                            />
                        </View>

                        <View
                            style={{
                                marginTop: 20,
                                display: 'flex',
                                alignItems: 'center',
                            }}
                        >
                            <DateTimePicker
                                style={{ backgroundColor: '#b3b3b3', width:'80%' }}
                                value={date}
                                mode="date"
                                placeholderText={useLocale({
                                    'en': 'Enter Birthday',
                                    'ru': 'Введите дату рождения',
                                })}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                            />
                        </View>

                        {errortext != '' ? (
                            <Text style={styles.errorTextStyle}>{errortext}</Text>
                        ) : null}

                        <TouchableOpacity
                            style={styles.buttonStyle}
                            activeOpacity={0.5}
                            onPress={handleSubmitButton}
                        >
                            <Text style={styles.buttonTextStyle}>{
                                useLocale({
                                    'en': 'REGISTER',
                                    'ru': 'ЗАРЕГИСТРИРОВАТЬСЯ',
                                })
                            }</Text>
                        </TouchableOpacity>

                        <Text
                            style={styles.loginTextStyle}
                            onPress={onLoginClick}
                        >
                            {
                                useLocale({
                                    'en': 'Already registered? Login',
                                    'ru': 'Уже зарегистрированы? Войти',
                                })
                            }
                        </Text>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        </>
    );
};
export default RegisterScreen;

const styles = StyleSheet.create({
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
    },
    buttonStyle: {
        color: '#FFFFFF',
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: Colors.Orange,
        borderWidth: 1,
        borderColor: Colors.LightOrange,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 15,
        textAlignVertical: 'center',
        alignItems: 'center',
        borderColor: '#dadae8',
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    },
    loginTextStyle: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        alignSelf: 'center',
        padding: 10,
    },
});
