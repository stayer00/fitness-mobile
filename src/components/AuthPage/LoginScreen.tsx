import React, { useState, createRef, FC } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    Keyboard,
    TouchableOpacity,
    KeyboardAvoidingView,
} from 'react-native';

import { useLocale } from '@hooks/useLocale';
import { AuthContext, AuthStore } from '@store/Auth';

import { useStore } from '@helpers/useStore';

import { Colors } from '@constants/color';

// import AsyncStorage from "@react-native-community/async-storage";

import Loader from '../common/Loader';
import Toast from '../common/Toast';

type LoginScreenProps = {
    onLoginSuccess: () => void;
    onRegisterClick: () => void;
};

const LoginScreen: FC<LoginScreenProps> = ({
    onLoginSuccess,
    onRegisterClick,
}) => {
    const { login } = process.env.NODE_ENV === 'test'
        ? { login: AuthStore.prototype.login }
        : useStore(AuthContext);

    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [errortext, setErrortext] = useState('');

    const passwordInputRef = createRef<any>();

    const handleSubmitPress = async () => {
        setErrortext('');
        
        try {
            setLoading(true);
            const loginResult = await login({
                email: userEmail,
                password: userPassword,
            });

            setLoading(false);
    
            if (loginResult) {
                onLoginSuccess();
            } else {
                setErrortext( useLocale({
                    'en': 'Login failed',
                    'ru': 'Ошибка входа',
                }));

                setTimeout(() => {
                    setErrortext('');
                }, 2500);
            }
        } catch (error) {
            setErrortext(error.message);

            setTimeout(() => {
                setErrortext('');
            }, 2500);
            setLoading(false);
        }
    };
    
    return (
        <View style={styles.mainBody}>
            <Loader loading={loading} />

            <Toast
                message={errortext}
                visible={Boolean(errortext)}
            />

            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                }}
            >
                <View>
                    <KeyboardAvoidingView enabled>
                        <View style={{ alignItems: 'center' }}>
                            <Image
                                source={require('../../assets/spinning.png')}
                                style={{
                                    width: '50%',
                                    height: 100,
                                    resizeMode: 'contain',
                                    margin: 30,
                                }}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                value={userEmail}
                                onChangeText={(UserEmail) => {
                                    console.log(UserEmail);
                                    setUserEmail(UserEmail);
                                }}
                                // placeholder='Enter Email'
                                placeholder={useLocale({
                                    'en': 'Enter Email',
                                    'ru': 'Введите email',
                                })} //dummy@abc.com
                                placeholderTextColor="#8b9cb5"
                                autoCapitalize="none"
                                keyboardType="email-address"
                                returnKeyType="next"
                                onSubmitEditing={() => passwordInputRef?.current?.focus()}
                                underlineColorAndroid="#f000"
                                blurOnSubmit={false}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                value={userPassword}
                                onChangeText={(UserPassword) => setUserPassword(UserPassword)}
                                placeholder="Enter Password" //12345
                                placeholder={useLocale({
                                    'en': 'Enter Password',
                                    'ru': 'Введите пароль',
                                })}
                                placeholderTextColor="#8b9cb5"
                                keyboardType="default"
                                ref={passwordInputRef}
                                onSubmitEditing={Keyboard.dismiss}
                                blurOnSubmit={false}
                                secureTextEntry={true}
                                underlineColorAndroid="#f000"
                                returnKeyType="next"
                            />
                        </View>
                        {/* {errortext != '' ? (
                            <Text style={styles.errorTextStyle}>{errortext}</Text>
                        ) : null} */}
                        <TouchableOpacity
                            style={styles.buttonStyle}
                            activeOpacity={0.5}
                            onPress={handleSubmitPress}
                        >
                            <Text style={styles.buttonTextStyle}>
                                {
                                    useLocale({
                                        'en': 'LOGIN',
                                        'ru': 'ВОЙТИ',
                                    })
                                }
                            </Text>
                        </TouchableOpacity>

                        <Text
                            style={styles.registerTextStyle}
                            onPress={onRegisterClick}
                        >
                            {
                                useLocale({
                                    'en': 'New Here ? Register',
                                    'ru': 'Первый раз? Зарегистрироваться',
                                })
                            }
                        </Text>
                    </KeyboardAvoidingView>
                </View>
            </ScrollView>
        </View>
    );
};
export default LoginScreen;

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.Black,
        alignContent: 'center',
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
    },
    buttonStyle: {
        backgroundColor: Colors.Orange,
        borderWidth: 1,
        color: '#FFFFFF',
        borderStyle: 'solid',
        borderColor: Colors.LightOrange,
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 25,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#dadae8',
    },
    registerTextStyle: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        alignSelf: 'center',
        padding: 10,
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
});
