import React, { useEffect } from 'react';
import { View, StyleSheet, Text, BackHandler } from 'react-native';

import { MaterialIcons } from '@expo/vector-icons';
import { useTooltip } from '@hooks/useTooltip';
import GestureRecognizer from 'react-native-swipe-gestures';
import Tooltip from 'react-native-walkthrough-tooltip';

import { Circle } from '@components/common/Circle';

import { Colors, PositionCircle } from '@constants/color';

import { TrainingAppointment } from 'interfaces/interfaces';

interface TrainingDayProps {
    trainingAppointments: TrainingAppointment[],
    onSetClient: (idx: number) => void;
    onUnsetClient: (idx: number) => void;
    onBackAction: () => void;
}

export const TrainingDay = ({
    trainingAppointments,
    onBackAction,
    onSetClient,
    onUnsetClient,
}: TrainingDayProps) => {
    const extractDateFromISO = (date: string) => new Date(date).toTimeString().match(/\d\d:\d\d/)![0] ?? '';

    useEffect(() => {
        const backAction = () => {
            onBackAction();
            return true;
        };
    
        const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);
    
        return () => backHandler.remove();
    }, []);

    const config = {
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80,
    };

    return (
        <>
            {trainingAppointments.map(({ service, nameCoach, date, currentClient, isVisited }, idx) => {
                const {
                    setTooltipVisible,
                    getTooltipConfig,
                } = useTooltip(`Тренировка была ${isVisited ? 'посещена' : 'пропущена'}`);

                return (
                    <View style={styles.rowTraining}>
                        <Text style={styles.infoItem}>{extractDateFromISO(date)}</Text>
                        <Text style={styles.infoItem}>{service}</Text>

                        <Text style={styles.infoItem}>{nameCoach}</Text>

                        {new Date(date).getTime() > Date.now() && (
                            <Circle
                                key={`circle-${currentClient ? 'unset' : 'set'}`}
                                position={PositionCircle.Center}
                                size={20}
                                color={currentClient ? Colors.Red : Colors.Green }
                                onClick={() => {
                                    if (currentClient) {
                                        onUnsetClient(idx);
                                    } else {
                                        onSetClient(idx);
                                    }
                                }}
                            >
                                <Text style={styles.plus}>{currentClient ? 'x' : '+' }</Text>
                            </Circle>
                        )}

                        {new Date(date).getTime() < Date.now() && Boolean(currentClient) && (
                            <Tooltip
                                {...getTooltipConfig()}
                            >
                                <MaterialIcons
                                    onPress={() => setTooltipVisible(true)}
                                    name={isVisited ? 'done' : 'remove'}
                                    size={24}
                                    color={isVisited ? 'green' : 'red'}
                                />
                            </Tooltip>
                        )}
                    </View>
                );
            })}

            <GestureRecognizer
                onSwipeRight={() => {
                    onBackAction();
                }}
                config={config}
                style={{
                    flex: 1,
                    // backgroundColor: 'red',
                }}
            />
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
    },
    rowTraining: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
    },
    infoItem: {
        color: Colors.Orange,
        borderWidth: 1,
        borderBottomColor: Colors.LightOrange,
        flex: 1,
        textAlign: 'center',
        marginRight: 10,
        marginLeft: 10,
        fontSize: 18,
    },
    plus: {
        fontSize: 15,
    },
});
