import { MultiDotMarking } from 'react-native-calendars';

import { TrainingAppointment } from '@interfaces/interfaces';

import { Colors } from '@constants/color';

export type MarkedDaеtes = {
    [date: string]: MultiDotMarking;
};

export const extractDateFromISO = (date: string) => date.slice(0, date.indexOf('T'));

export const filterTrainings = (
    trainingAppointments: TrainingAppointment[],
    selectedDay: string | null,
) => {
    return trainingAppointments.filter(({ date }) => extractDateFromISO(date) === selectedDay);
};

export const needRedDot = (
    handledMarkedDates: MarkedDaеtes,
    dateString: string,
    training: TrainingAppointment,
) => handledMarkedDates[dateString]?.dots[0]?.color !== Colors.Red && !training.currentClient;

export const needGreenDot = (
    handledMarkedDates: MarkedDaеtes,
    dateString: string,
    training: TrainingAppointment,
) => handledMarkedDates[dateString]?.dots[1]?.color !== Colors.Green && training.currentClient;

const addEmptyValueIfNeed = (
    handledMarkedDates: MarkedDaеtes,
    dateString: string,
) => {
    if (!handledMarkedDates[dateString]) {
        handledMarkedDates[dateString] = {
            disabled: false,
            dots: [],
            selected: false,
        };
    }
};

export const defineDots = (trainingAppointments: TrainingAppointment[]) => {
    const handledMarkedDates: MarkedDaеtes = {};

    trainingAppointments.forEach((training) => {
        const dateString = extractDateFromISO(training.date);

        addEmptyValueIfNeed(handledMarkedDates, dateString);

        console.log('dateString', dateString);
        
        if (needRedDot(handledMarkedDates, dateString, training)) {
            handledMarkedDates[dateString].dots[0] = {
                key: training.service,
                color: Colors.Red,
                selectedDotColor: Colors.Red,
            };
        } else if (needGreenDot(handledMarkedDates, dateString, training)) {
            if (!handledMarkedDates[dateString].dots[0]) {
                handledMarkedDates[dateString].dots[0] = {
                    key: training.service,
                    color: Colors.Black,
                    selectedDotColor: Colors.Black,
                };
            }
            handledMarkedDates[dateString].dots[1] = {
                key: training.service,
                color: Colors.Green,
                selectedDotColor: Colors.Green,
            };
        }
    });

    return handledMarkedDates;
};