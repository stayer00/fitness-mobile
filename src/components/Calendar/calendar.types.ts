import { TrainingDayProps } from './TrainingDay';

export type RootStackParamList = {
    Calendar: undefined;
    TrainingDay: TrainingDayProps;
};

