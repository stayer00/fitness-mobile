import React, { useEffect, useMemo, useState } from 'react';
import { View, StyleSheet } from 'react-native';

import { ClientContext } from '@store/Clients';
import { TrainingAppointmentContext } from '@store/TrainingAppointment';
import { observer } from 'mobx-react';
import { Calendar } from 'react-native-calendars';

import { useStore } from '@helpers/useStore';

import { Colors } from '@constants/color';

import Toast from '../common/Toast';
import { defineDots, filterTrainings, MarkedDaеtes } from './helper';
import { TrainingDay } from './TrainingDay';

export const CalendarView = observer((
) => {
    const {
        trainingAppointments,
        state,
        loadClientTrainingAppointments,
        setClientToTrainingAppointment,
        unsetClientToTrainingAppointment,
    } = useStore(TrainingAppointmentContext);

    const {
        client,
    } = useStore(ClientContext);

    const [errortext, setErrortext] = useState('');
    // console.log('trainingAppointmentstrainingAppointments', trainingAppointments);
    
    const [markedDates, setMarkedDates] = useState<MarkedDaеtes>({});

    // const [filteredTrainingsByDay, setFilteredTrainingsByDay] = useState<TrainingAppointment[]>([]);

    const [selectedDay, setSelectedDay] = useState<string | null>(null);

    const filteredTrainingsByDay = useMemo(() => (
        filterTrainings(trainingAppointments, selectedDay)
    ), [state, selectedDay, trainingAppointments]);

    useEffect(() => {
        // trainingAppointments.splice(trainingAppointments.length);
        const handledMarkedDates = defineDots(trainingAppointments);
        setMarkedDates({ ...handledMarkedDates });
    }, [state === 'loaded']);

    useEffect(() => {
        if (state === 'error') {
            setErrortext('Trainings weren’t received');
            
            setTimeout(() => {
                setErrortext('');
            }, 2500);
        }
    }, [state]);
    
    const handleOnSetClient = async (idxSeletedTraining: number) => {
        const training = filteredTrainingsByDay[idxSeletedTraining];
        const stId = client?.seasonTickets.find(i => i.serviceId === training?.serviceId);

        await setClientToTrainingAppointment(training?.id, stId?.id || 0);
    };

    const handleOnUnSetClient = async (idxSeletedTraining: number) => {
        const training = filteredTrainingsByDay[idxSeletedTraining];
        const stId = client?.seasonTickets.find(i => i.serviceId === training?.serviceId);

        await unsetClientToTrainingAppointment(filteredTrainingsByDay[idxSeletedTraining]?.id, stId?.id ?? 0);
    };

    return (
        <View style={styles.container}>
            <Toast
                message={errortext}
                visible={Boolean(errortext)}
            />

            {Boolean(filteredTrainingsByDay.length) && (
                <TrainingDay
                    onBackAction={() => setSelectedDay(null)}
                    // onBackAction={() => setFilteredTrainingsByDay([])}
                    trainingAppointments={filteredTrainingsByDay}
                    onSetClient={handleOnSetClient}
                    onUnsetClient={handleOnUnSetClient}
                />
            )}

            {Boolean(!filteredTrainingsByDay.length) && (
                <Calendar
                    markingType="multi-dot"
                    style={{
                        borderWidth: 1,
                        borderColor: Colors.DisabledOrange,
                    }}
                    enableSwipeMonths={true}
                    // onDayPress={onDaySelect}
                    onMonthChange={({ month }) => loadClientTrainingAppointments(client!.id, month)}
                    onDayPress={({ dateString }) => setSelectedDay(dateString)}
                    theme={{
                        backgroundColor: Colors.LightOrange,
                        calendarBackground: 'transparent',
                        textSectionTitleColor: Colors.Orange,
                        textSectionTitleDisabledColor: '#d9e1e8',
                        selectedDayBackgroundColor: '#00adf5',
                        selectedDayTextColor: '#ffffff',
                        todayTextColor: Colors.Orange,
                        dayTextColor: Colors.LightOrange,
                        textDisabledColor: Colors.DisabledOrange,
                        dotColor: '#00adf5',
                        selectedDotColor: '#ffffff',
                        arrowColor: 'orange',
                        disabledArrowColor: '#d9e1e8',
                        monthTextColor: Colors.Orange,
                        indicatorColor: Colors.Orange,
                        textDayFontFamily: 'monospace',
                        textMonthFontFamily: 'monospace',
                        textDayHeaderFontFamily: 'monospace',
                        textDayFontWeight: '300',
                        textMonthFontWeight: 'bold',
                        textDayHeaderFontWeight: '300',
                        textDayFontSize: 16,
                        textMonthFontSize: 16,
                        textDayHeaderFontSize: 16,
                    }}
                    markedDates={markedDates}
                />
            )}
        </View>
    );
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
    },
    selectedMonth: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: Colors.Orange,
    },
});
