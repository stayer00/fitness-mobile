import React, {
    FC,
    useEffect,
    useMemo,
    useState,
} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    ScrollView,
    TouchableWithoutFeedback,
    TouchableHighlight,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';

import useAddSeasonTickerHooks from '@hooks/useAddSeasonTickerHooks';
import useGetPrice from '@hooks/useGetPrice';
import { useLocale } from '@hooks/useLocale';
import { num_word } from '@hooks/useNumWord';
import DateTimePicker from '@react-native-community/datetimepicker';
import { ClientContext } from '@store/Clients';
import { CoachesContext } from '@store/Coaches';
import { ServicesContext } from '@store/Services';
import { observer } from 'mobx-react';

import CarouselComponent from '@components/common/Carousel';
import { Dropdown } from '@components/common/Dropdown';
import Loader from '@components/common/Loader';
import { SuccessView } from '@components/common/SuccessView';
import Overlay from '@components/Overlay';

import { useStore } from '@helpers/useStore';

import { Colors } from '@constants/color';

import Toast from '../common/Toast';

const deviceWidth = Dimensions.get('window').width;

const coach = require('../../assets/default-profile.png');
const placeholder = require('../../assets/placeholder.png');

interface AddNewSeasonTicketProps {
    modalVisible: boolean;
    onCloseModal: () => void;
}

const AddNewSeasonTicket: FC<AddNewSeasonTicketProps> = observer(
    ({ modalVisible, onCloseModal }) => {
        const { services, tickets, state: stateServices, getPrice } = useStore(ServicesContext);
        const { coaches, state: stateCoaches } = useStore(CoachesContext);
        const { state: stateClient, client } = useStore(ClientContext);
        const [dateStart, setDateStart] = useState(
            new Date(new Date().setHours(0, 0, 0, 0)),
        );

        const {
            createNewSt,
            setIsSuccess,
            isSuccess,
            errortext,
        } = useAddSeasonTickerHooks({ onCloseModal });

        const {
            price,
            loading: loadingPrice,
            selectedTicket,
            setSelectedTicket,
            selectedService,
            setSelectedService,
        } = useGetPrice({
            services,
            tickets,
            // @ts-ignore
            getPrice: (...arg) => getPrice(...arg, client?.id),
        });

        const serviceImages = useMemo(
            () =>
                services.map(({ id, image }) => ({
                    id: id.toString(),
                    image: image || placeholder,
                })),
            [services.length],
        );

        const [indexSelectedService, setIndexSelectedService] = useState<number>(0);
        const [indexSelectedCoach, setIndexSelectedCoach] = useState<number>(0);

        useEffect(() => {
            setIndexSelectedCoach(0);
        }, [selectedService]);

        const filteredCoaches = useMemo(
            () =>
                coaches.filter(({ activities }) =>
                    Boolean(
                        activities.find(
                            ({ idService }) =>
                                idService === selectedService?.id,
                        ),
                    ),
                ),
            [selectedService?.id],
        );

        const coachImages = useMemo(
            () =>
                filteredCoaches.map(({ image }, idx) => ({
                    id: idx.toString(),
                    image: image || coach,
                })),
            [selectedService?.id],
        );
        
        const loading = stateClient === 'loading'
            || stateCoaches === 'loading'
            || stateServices === 'loading';
            
        const onCreateST = async () => {
            const {
                count,
                duration,
            } = tickets[selectedTicket];

            await createNewSt(
                selectedService?.id ?? -1,
                selectedService?.name === 'Персональная тренировка' ? filteredCoaches[indexSelectedCoach]?.id : null,
                duration,
                count,
                selectedService!,
                dateStart,
                selectedService?.name === 'Персональная тренировка' ? filteredCoaches[indexSelectedCoach] : null,
            );
        };

        const getTicketMsg = (
            duration: number,
            count: number,
        ) => (
            count !== 1
                ? `${duration} ${num_word(duration, ['месяц', 'месяца', 'месяцев'])} - ${count} ${num_word(count, ['занятие', 'занятия', 'занятий'])}`
                : 'Разовое посещение'
        );
    
        return (
            <>
                <Loader loading={loading} />

                <Toast
                    message={errortext}
                    visible={Boolean(errortext)}
                />

                <Toast
                    message={errortext}
                    visible={Boolean(errortext)}
                />

                <SuccessView
                    text={useLocale({
                        'en': 'Season ticket was created',
                        'ru': 'Абонемент успешно создан',
                    })}
                    visible={isSuccess}
                    onClose={() => {
                        setIsSuccess(false);
                    }}
                />

                <Overlay
                    visible={modalVisible}
                    closeCallback={onCloseModal}
                    transparent={false}
                    // isVisible={modalVisible}
                    // deviceWidth={deviceWidth}
                    // deviceHeight={deviceHeight}
                    // propagateSwipe={true}
                    // onBackdropPress={onCloseModal}
                >
                    <View style={[styles.centeredView, styles.modalView]}>
                        {/* ... */}
                        <ScrollView
                            contentContainerStyle={[
                                {
                                    flexGrow: 1,
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    // backgroundColor: Colors.Green
                                },
                            ]}
                        >
                            <TouchableHighlight>
                                <TouchableWithoutFeedback>
                                    <View style={[styles.centeredView, styles.wrapper]}>
                                        <CarouselComponent
                                            images={serviceImages}
                                            width={deviceWidth * 0.6}
                                            indexSelected={indexSelectedService}
                                            // paginationStyles={{ marginTop: -90 }}
                                            onSwipe={(idx) => {
                                                setIndexSelectedService(idx);
                                                setSelectedService(services[idx]);
                                            }}
                                        />

                                        <Text style={styles.textCarousel}>
                                            {selectedService?.name ?? ''}
                                        </Text>

                                        {selectedService?.name === 'Персональная тренировка' && (
                                            <>
                                                <CarouselComponent
                                                    images={coachImages}
                                                    width={deviceWidth * 0.6}
                                                    indexSelected={indexSelectedCoach}
                                                    // paginationStyles={{ marginTop: -90 }}
                                                    onSwipe={setIndexSelectedCoach}
                                                />

                                                <Text style={styles.textCarousel}>
                                                    {filteredCoaches[indexSelectedCoach]?.surname}
                                                </Text>
                                            </>)}

                                        <Text style={[{ marginTop: 50 }, styles.textStyle]}>
                                            {
                                                useLocale({
                                                    'en': 'Ticket:',
                                                    'ru': 'Тип абонемента:',
                                                })
                                            }
                                        </Text>
                                        <View
                                            style={styles.borderForDropdown}
                                        >
                                            <Dropdown
                                                title={
                                                    getTicketMsg(tickets[selectedTicket]?.duration, tickets[selectedTicket]?.count)
                                                        .split('-').join('\n')
                                                }
                                                values={tickets.map(({ duration, count }, idx) => ({
                                                    key: idx,
                                                    title: getTicketMsg(duration, count),
                                                }))}
                                                selectedValue={selectedTicket}
                                                onSelectedNewValue={(val) => setSelectedTicket(val as number)}
                                            />
                                        </View>

                                        <Text style={[{ marginTop: 50 }, styles.textStyle]}>
                                            {
                                                useLocale({
                                                    'en': 'Date start:',
                                                    'ru': 'Дата начала:',
                                                })
                                            }
                                        </Text>

                                        <DateTimePicker
                                            testID="dateTimePicker"
                                            style={{ backgroundColor: '#b3b3b3', width:'80%', marginTop: 20 }}
                                            value={dateStart}
                                            minimumDate={new Date(new Date().setHours(0, 0, 0, 0))}
                                            mode="date"
                                            is24Hour={true}
                                            onChange={(_: any, newCurrentDate: Date) => setDateStart(newCurrentDate)}
                                        />

                                        {loadingPrice && (
                                            <ActivityIndicator style={{ marginTop: 20 }} size="large" />
                                        )}
                                        {!loadingPrice && (
                                            <Text style={[styles.textCarousel, { marginTop: 20 }]}>
                                                {`Стоимость: ${price ?? ''} рублей`}
                                            </Text>
                                        )}

                                        {/* <Text style={[{ marginTop: 30 }, styles.textStyle]}>
                                            {
                                                useLocale({
                                                    'en': 'Count classes:',
                                                    'ru': 'Количество занятий:',
                                                })
                                            }
                                        </Text> */}
                                        {/* <View
                                            style={[styles.borderForDropdown, { marginBottom: !errortext ? 40 : 20 }]}
                                        >
                                            <Dropdown
                                                title={useLocale({
                                                    'en': 'Please select count classes',
                                                    'ru': 'Пожалуйста, введите кол-во занятий',
                                                })}
                                                values={getNumberOfPeriods(30)}
                                                selectedValue={selectedCountClasses}
                                                onSelectedNewValue={setSelectedCountClasses}
                                            />
                                        </View> */}

                                        {errortext != '' ? (
                                            <View
                                                style={{ marginBottom: 40 }}
                                            >
                                                <Text style={styles.errorTextStyle}>{errortext}</Text>
                                            </View>
                                        ) : null}

                                        <TouchableOpacity
                                            activeOpacity={0.65}
                                            onPress={onCreateST}
                                            style={styles.button}
                                            disabled={!selectedService}
                                        >
                                            <Text style={{ textAlign: 'center', fontSize: 20 }}>
                                                {
                                                    useLocale({
                                                        'en': 'Pay',
                                                        'ru': 'Оплатить',
                                                    })
                                                }
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableWithoutFeedback>
                            </TouchableHighlight>
                        </ScrollView>
                    </View>
                </Overlay>
            </>
        );
    },
);

const styles = StyleSheet.create({
    centeredView: {
        justifyContent: 'center',
        // alignItems: 'center',
    },
    modalView: {
        margin: 15,
        display: 'flex',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 40,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        // elevation: 5,
        justifyContent: 'center',
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        // height: deviceHeight * 1.3,
        paddingBottom: 30,
    },
    button: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Colors.Orange,
        elevation: 2,
        width: '50%',
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        backgroundColor: Colors.Orange,
        height: 35,
        marginTop: 30,
    // shadowColor: Colors.Orange,
    },
    textCarousel: {
        textAlign: 'center',
        textTransform: 'uppercase',
        alignItems: 'center',
        // marginVertical: -20,
        color: 'black',
    },
    textStyle: {
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
        textTransform: 'uppercase',
        alignItems: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
    borderForDropdown: {
        borderStyle: 'solid',
        borderColor: Colors.Orange,
        borderWidth: 1,
        borderRadius: 15,
        width: '60%',
        justifyContent: 'center',
        display: 'flex',
        alignItems: 'center',
        marginTop: 10,
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
});

export default AddNewSeasonTicket;
