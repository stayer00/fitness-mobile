import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';

import { useTooltip } from '@hooks/useTooltip';
import Tooltip from 'react-native-walkthrough-tooltip';

import { Colors } from '@constants/color';

import { SeasonTicket } from 'interfaces/interfaces';

interface SeasonTicketItemProp {
    seasonTicket: SeasonTicket;
    idx: number;
}

export const SeasonTicketItem = ({
    seasonTicket: { classesLeft, classesTotal, service, dateEnd },
    idx,
}: SeasonTicketItemProp) => {
    const getEndDay = () => {
        const date = new Date(dateEnd);

        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();

        return `${day} - ${month} - ${year}`;
    };

    const {
        setTooltipVisible: setTooltipClassesVisible,
        getTooltipConfig: getTooltipClassesConfig,
    } = useTooltip(`Осталось ${classesLeft} посещения из ${classesTotal}`);

    const {
        setTooltipVisible: setToolTipDateVisible,
        getTooltipConfig: getTooltipDateConfig,
    } = useTooltip('Дата окончания действия абонемента');

    return (
        <TouchableOpacity
            style={styles.container}
            activeOpacity={0.95}
            key={`season-ticket-key-${idx}`}
        >
            <Tooltip
                {...getTooltipClassesConfig()}
            >
                <Text onPress={() => setTooltipClassesVisible(true)}>
                    {`${classesLeft} / ${classesTotal}`}
                </Text>
            </Tooltip>

            <Text style={styles.service}>{service.name.split(' ').join('\n')}</Text>

            <Tooltip
                {...getTooltipDateConfig()}
            >
                <Text onPress={() => setToolTipDateVisible(true)}>
                    {`${getEndDay()}`}
                </Text>
            </Tooltip>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 22,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.LightOrange,
        alignItems: 'center',
        marginBottom: 20,
        borderRadius: 10,
    },
    service: {
        fontSize: 18,
        fontWeight: 'bold',
    },
});
