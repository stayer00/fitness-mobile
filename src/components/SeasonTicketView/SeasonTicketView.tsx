import React, { useEffect, useState } from 'react';
import { SafeAreaView, ScrollView, Text, StyleSheet } from 'react-native';

import { AuthContext } from '@store/Auth';
import { ClientContext } from '@store/Clients';
import { CoachesContext } from '@store/Coaches';
import { ServicesContext } from '@store/Services';
import { observer } from 'mobx-react';

import { Circle } from '@components/common/Circle';
import Loader from '@components/common/Loader';

import { useStore } from '@helpers/useStore';

import Toast from '../common/Toast';
import AddNewSeasonTicket from './AddNewSeasonTicket';
import { SeasonTicketItem } from './SeasonTicketItem';

export const SeasonTicketView = observer(() => {
    const { state, client, loadClientData } = useStore(ClientContext);
    const { userId } = useStore(AuthContext);
    const { services, loadServices } = useStore(ServicesContext);
    const { coaches, loadCoaches } = useStore(CoachesContext);

    const [errortext, setErrortext] = useState('');

    const [showAddSeasonTicket, setShowAddSeasonTicket] = useState<boolean>(false);

    useEffect(() => {
        if (!client && userId) {
            loadClientData(userId);
        }

        if (!services.length) {
            loadServices();
        }

        if (!coaches.length) {
            loadCoaches();
        }
    }, []);

    useEffect(() => {
        if (state === 'error') {
            setErrortext('Season tickets weren’t received');

            setTimeout(() => {
                setErrortext('');
            }, 2500);
        }
    }, [state]);

    return (
        <SafeAreaView style={styles.container}>
            <Loader loading={state === 'loading'} />

            <Toast
                message={errortext}
                visible={Boolean(errortext)}
            />

            <ScrollView style={styles.scrollView}>
                {client?.seasonTickets?.map((item, idx) => (
                    <SeasonTicketItem seasonTicket={item} idx={idx} />
                ))}
            </ScrollView>

            <AddNewSeasonTicket
                modalVisible={showAddSeasonTicket}
                onCloseModal={() => setShowAddSeasonTicket(false)}
            />

            <Circle onClick={() => setShowAddSeasonTicket(true)}>
                <Text style={styles.plus}>+</Text>
            </Circle>
        </SafeAreaView>
    );
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
    },
    scrollView: {
        marginHorizontal: 20,
        marginTop: 10,
    },
    text: {
        fontSize: 42,
    },
    plus: {
        fontSize: 35,
    },
});
