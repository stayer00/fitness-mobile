import React, { useEffect, useMemo, useState } from 'react';
import { View, StyleSheet, Dimensions, Text } from 'react-native';

import { ServicesContext } from '@store/Services';
import { observer } from 'mobx-react';

import CarouselComponent from '@components/common/Carousel';

import { useStore } from '@helpers/useStore';

import { Colors } from '@constants/color';

import Toast from '../common/Toast';

const placeholder = require('../../assets/placeholder.png');

export const ServicesView = observer(() => {
    const { width } = Dimensions.get('window');

    const [errortext, setErrortext] = useState('');

    const { services, state } = useStore(ServicesContext);

    const images = useMemo(() => services.map(({ id, image }) => ({
        id: id.toString(),
        image: image || placeholder,
    })), [services.length]);

    const [indexSelected, setIndexSelected] = useState(0);

    const onSelect = (handledIndexSelected: number) => {
        setIndexSelected(handledIndexSelected);
    };

    const getServiceName = () => services[indexSelected] ? services[indexSelected].name : 'Empty';

    const getServiceCost = () => services[indexSelected] ? services[indexSelected].cost : -1;
    
    useEffect(() => {
        if (state === 'error') {
            setErrortext('Season tickets weren’t received');

            setTimeout(() => {
                setErrortext('');
            }, 2500);
        }
    }, [state]);

    return (
        <View style={styles.container}>
            {/* Title JSX Remains same */}
            {/* Carousel View */}
            <Toast
                message={errortext}
                visible={Boolean(errortext)}
            />

            <View style={styles.carousel}>
                <CarouselComponent
                    images={images}
                    width={width}
                    indexSelected={indexSelected}
                    onSwipe={onSelect}
                />

                <View style={styles.trainingInfo}>
                    <Text style={styles.trainingInfoItem}>{getServiceName()}</Text>

                    <Text style={styles.trainingInfoItem}>{getServiceCost()}</Text>
                </View>
            </View>
        </View>
    );
});

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: 'black', alignItems: 'center', marginTop: 40, display: 'flex', flexDirection: 'column' },
    carousel: { flex: 0.8, marginTop: 20 },
    image: { width: '100%', height: '100%' },
    trainingInfo: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
    },
    trainingInfoItem: {
        color: Colors.Orange,
        fontSize: 20,
        borderBottomColor: Colors.LightOrange,
        borderWidth: 1,
        width: '45%',
        marginLeft: 10,
        marginRight: 10,
        textAlign: 'center',
    },
});
