import React, { FC } from 'react';
import {
    Modal,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    ViewStyle,
    ScrollView,
    View,
} from 'react-native';

import { Colors } from '@constants/color';

interface OverlayProps {
    style?: ViewStyle,
    transparent?: boolean,
    animationType?: 'fade' | 'none' | 'slide',
    visible: boolean,
    closeCallback?: () => void,
}

const Overlay: FC<OverlayProps> = ({
    style = {},
    transparent = true,
    animationType = 'fade',
    visible,
    closeCallback = () => null,
    children,
}) => (
    <Modal
        animationType={animationType}
        transparent={transparent}
        visible={visible}
        onRequestClose={closeCallback}
    >
        <KeyboardAvoidingView
            style={{ flex: 1, zIndex: 100000 }}
            enabled
        >
            <ScrollView
                contentContainerStyle={[
                    {
                        flexGrow: 1,
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        backgroundColor: Colors.GrayDark,
                    },
                    style,
                ]}
            >
                <TouchableWithoutFeedback onPress={closeCallback}>
                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                            opacity: 0.32,
                        }}
                    />
                </TouchableWithoutFeedback>
    
                <>
                    {children}
                </>
            </ScrollView>
        </KeyboardAvoidingView>
    </Modal>
)
;

export default Overlay;