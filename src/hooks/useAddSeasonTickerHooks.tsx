import { useState } from 'react';

import { AuthContext } from '@store/Auth';
import { ClientContext, ClientStore } from '@store/Clients';

import { Coach, Service } from '@interfaces/interfaces';

import { useStore } from '@helpers/useStore';

import { useLocale } from './useLocale';

type Props = {
    onCloseModal: () => void;
};

const useAddSeasonTickerHooks = ({ onCloseModal }: Props) => {
    const { createStRequest } = process.env.NODE_ENV === 'test'
        ? { createStRequest: ClientStore.prototype.createStRequest }
        : useStore(ClientContext);
    const { userId } = process.env.NODE_ENV === 'test'
        ? { userId: 'userId' }
        : useStore(AuthContext);

    const [isSuccess, setIsSuccess] = useState<boolean>(false);
    const [errortext, setErrortext] = useState('');

    const createNewSt = async (
        serviceId: number,
        coachId: string | null,
        selectedTimePeriod: number,
        selectedCountClasses: number,
        service: Service,
        dateStart: Date,
        coach?: Coach | null,
    ) => {
        const result = await createStRequest(
            userId, {
            // clientId: userId,
                serviceId,
                coachId,
                dateStart: dateStart.toISOString(),
                dateEnd: new Date(
                    (
                        new Date(dateStart).getTime()
                        + Number(selectedTimePeriod) * 1000 * 60 * 60 * 24 * (Number(selectedCountClasses) === 1 ? 1 : 31)
                    ),
                ).toISOString(),
                classesTotal: Number(selectedCountClasses),
                classesLeft: Number(selectedCountClasses),
                // totalCost: number,
                count: 0,
            },
            service,
            coach ?? null,
        );

        if (result >= 200 && result < 300) {
            setIsSuccess(true);
            onCloseModal();
        } else {
            if (result === 402) {
                setErrortext(
                    useLocale({
                        'en': 'Not enough money',
                        'ru': 'Недостаточно средств',
                    }),
                );
            } else {
                setErrortext(
                    useLocale({
                        'en': 'Error while buy season ticket',
                        'ru': 'Ошибка во время покупки абонемента',
                    }),
                );
            }

            setTimeout(() => {
                setErrortext('');
            }, 2500);
        }
    };

    return { createNewSt, setIsSuccess, isSuccess, errortext };
};

export default useAddSeasonTickerHooks;