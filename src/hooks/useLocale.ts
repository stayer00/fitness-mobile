import * as Localization from 'expo-localization';

export type AppLocaleType = 'ru' | 'en';

export const getLocale = () => {
    const currLocale = Localization.locale.slice(0, 2) as AppLocaleType;
    // fallback
    const locale = ['ru', 'en'].includes(currLocale) ? currLocale : 'en';

    return locale;
};

export const useLocale = <T = string>(texts: Record<AppLocaleType, T>) => {
    const currLocale = getLocale();

    const hasCurrLocale = Object.keys(texts).includes(currLocale);

    if (hasCurrLocale) {
        return texts[currLocale];
    }

    // fallback locale
    return texts.en;
};
