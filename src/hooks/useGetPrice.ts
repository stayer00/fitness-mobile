import { useEffect, useMemo, useState } from 'react';

import { Ticket, Service } from '@interfaces/interfaces';

type UseGetPriceProps = {
    tickets: Array<Ticket>,
    services: Array<Service>,
    getPrice: (count: number, duration: number, serviceId: number) => Promise<number>,
};

const useGetPrice = ({
    tickets = [],
    services = [],
    getPrice,
}: UseGetPriceProps) => {
    const [selectedTicket, setSelectedTicket] = useState<number>(0);
    const [selectedService, setSelectedService] = useState<Service | null>(services[0] || null);
    const [price, setPrice] = useState(0);

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        if ((selectedService?.id ?? -1) >= 0 && selectedTicket !== -1) {
            getPrice(
                tickets[selectedTicket]!.count,
                tickets[selectedTicket]!.duration,
                selectedService!.id!,
            ).then((resultPrice) => {
                setPrice(resultPrice);
                setLoading(false);
            }).catch((e) => {
                setLoading(false);
            });
        }
    }, [selectedTicket || selectedService?.id]);

    return {
        loading,
        price,
        selectedService,
        setSelectedService,
        selectedTicket,
        setSelectedTicket,
    };
};

export default useGetPrice;
