import React, { useState } from 'react';
import { Text } from 'react-native';

export const useTooltip = (text: string) => {
    const [tooltipVisible, setTooltipVisible] = useState(false);

    const getTooltipConfig = () => ({
        animated: true,
        arrowSize: { width: 16, height: 8 },
        backgroundColor: 'rgba(0,0,0,0.5)',
        isVisible: tooltipVisible,
        content: <Text>{text}</Text>,
        placement: 'bottom' as 'bottom',
        onClose: () => setTooltipVisible(false),
    });

    return ({
        tooltipVisible,
        getTooltipConfig,
        setTooltipVisible,
    });
};
