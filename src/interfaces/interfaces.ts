export interface Service {
    id: number,
    name: string,
    cost: number,
    image: string,
}

export interface SeasonTicket {
    id: number,
    clientId: string,
    service: Service,
    serviceId: number,
    coach: string,
    coachId?: string | null,
    dateStart: string,
    dateEnd: string,
    classesTotal: number,
    classesLeft: number,
    totalCost: number,
    count: number,
    visitings: string[],
}

export interface TrainingAppointment {
    id: number,
    service: string
    serviceId: number,
    clients: ClientTraining[],
    date: string,
    nameCoach: string,
    coachId: string,
    loungeId: number,
    nameLounge: string,
    isVisited?: boolean,
    currentClient?: boolean,
    isCanceled: boolean,
}

export interface ClientTraining {
    id: number;
    seasonTicketId: number,
    clientId: string,
    userName: string,
    isVisited?: boolean,
}

export type Gender = 'Woman' | 'Man';

export const Gender: { [key: string]: Gender } = {
    Woman: 'Женский',
    Man: 'Мужской',
};

export interface Client {
    id: string;
    surname: string;
    name: string;
    birthday: string;
    gender: Gender;
    numberPhone: string;
    balance?: number;
    expanded?: boolean;
    seasonTickets: SeasonTicket[],
}

import { Gender, IService } from '../Clients';

export interface Coach {
    id: string;
    surname: string;
    name: string;
    birthday: string;
    gender: Gender;
    numberPhone: string;
    // address: string;
    // getStarted: string;
    // baseSalary: number;
    // employmentRecordNumber: string;
    // itn: string;
    // passport: string;
    // snils: string;
    image: string,
    activities: IService[],
    expanded: boolean,
    editActivity: boolean,
}

export interface ICredential {
    cvc: string,
    expiry: string,
    name: string,
    number: string,
    balance: number,
}

export type Ticket = {
    duration: number,
    count: number,
};
