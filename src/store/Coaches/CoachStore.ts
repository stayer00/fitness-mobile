import { observable, action, makeObservable } from 'mobx';

import { Coach } from '@interfaces/interfaces';

import { fetchFunctionApi } from '../../helpers/fetchFunction';

export class CoachesStore {
    public coaches: Coach[] = [];

    public state: 'loading' | 'loaded' | 'error' = 'loading';

    constructor() {
        makeObservable(this, {
            coaches: observable,
            state: observable,
            loadCoaches: action.bound,
        });
    }

    public async loadCoaches() {
        this.state = 'loading';

        try {
            const coaches = await fetchFunctionApi<Coach[]>('/coaches');

            if (coaches && Array.isArray(coaches)) {
                this.coaches.push(...coaches);
                this.state = 'loaded';

            } else {
                console.error('Could not get coaches from the response payload.');
                this.state = 'error';
            }
        } catch (error) {
            console.error('Unexpected error has occurred when loading coaches.', error);
            this.state = 'error';
        }
    }
}

