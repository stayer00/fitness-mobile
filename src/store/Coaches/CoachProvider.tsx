import React, { createContext } from 'react';

import { useLocalStore } from 'mobx-react';

import { CoachesStore } from './CoachStore';

export const CoachesContext = createContext<CoachesStore | null>(null);

export const CoachesProvider = ({ children }: { children: React.ReactNode }) => {
    const store = useLocalStore(() => new CoachesStore());
    return <CoachesContext.Provider value={store}>{children}</CoachesContext.Provider>;
};
