import React, { createContext } from 'react';

import { useLocalStore } from 'mobx-react';

import { ClientStore } from './ClientsStore';

export const ClientContext = createContext<ClientStore | null>(null);

export const ClientProvider = ({ children }: { children: React.ReactNode }) => {
    const store = useLocalStore(() => new ClientStore());
    return <ClientContext.Provider value={store}>{children}</ClientContext.Provider>;
};
