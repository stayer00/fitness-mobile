import { observable, action, makeObservable } from 'mobx';

import { Client, Coach, ICredential, SeasonTicket, Service } from '@interfaces/interfaces';

import { fetchFunctionApi, postFunctionApi } from '../../helpers/fetchFunction';

export class ClientStore {
    public state: 'loading' | 'loaded' | 'error' = 'loading';

    public client: Client | null = null;

    public canGetOperation: boolean = true;

    constructor() {
        makeObservable(this, {
            state: observable,
            client: observable,
            canGetOperation: observable,
            loadClientData: action.bound,
        });
    }
    
    public async loadClientData(id: string) {
        try {
            const client = await fetchFunctionApi<Client>(`/clients/${id}`);
            
            if (client) {
                this.client = { ...client };
                this.state = 'loaded';
            } else {
                console.error('Could not get client from the response payload.');
                this.state = 'error';
            }
        } catch (error) {
            console.error('Unexpected error has occurred when loading clients.', error);
            this.state = 'error';
        }
    }

    public createStRequest = async (
        clientId: string,
        st: Partial<SeasonTicket>,
        selectedService: Service,
        selectedCoach: Coach | null,
    ) => {
        try {
            type StT = { id: number, cost: number };
            const answer = await postFunctionApi<StT>(`/clients/${clientId}/season-ticket/`, st);

            const { cost, id } = answer;
            st.id = id;
            st.visitings = [];
            st.service = selectedService;
            st.coach = selectedCoach?.name;

            this.client?.seasonTickets.push(st as SeasonTicket);

            if (this.client) {
                this.client.balance = (this.client.balance ?? 0) - cost;
            }
            return 200;
        } catch (error) {
            console.log('Unexpected error has occurred when adding st.', error);
            return error;
        }
    };

    public pay = async (pay: ICredential) => {
        try {
            if (this.client?.id) {
                await postFunctionApi<any>(`/clients/${this.client.id}/pay`, pay);
                return true;
            }
            return false;
        } catch (error) {
            console.error('Unexpected error has occurred when adding balance.', error);
            return false;
        }
    };
}

