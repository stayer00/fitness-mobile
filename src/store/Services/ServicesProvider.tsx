import React, { createContext } from 'react';

import { useLocalStore } from 'mobx-react';

import { ServicesStore } from './ServicesStore';

export const ServicesContext = createContext<ServicesStore | null>(null);

export const ServicesProvider = ({ children }: { children: React.ReactNode }) => {
    const store = useLocalStore(() => new ServicesStore());
    return <ServicesContext.Provider value={store}>{children}</ServicesContext.Provider>;
};
