import { action, makeObservable, observable } from 'mobx';

import { Service, Ticket } from '@interfaces/interfaces';

import { fetchFunctionApi, postFunctionApi } from '../../helpers/fetchFunction';

export class ServicesStore {
    public services: Service[] = [];

    public tickets: Ticket[] = [];

    public state: 'loading' | 'loaded' | 'error' = 'loading';

    constructor() {
        makeObservable(this, {
            services: observable,
            state: observable,
            loadServices: action.bound,
        });
    }

    public async loadServices() {
        this.state = 'loading';

        try {
            const servicesResult = await fetchFunctionApi<{
                services: Service[],
                tickets: Ticket[],
            }>('/services');

            if (servicesResult) {
                this.services = [];
                this.tickets = [];

                this.services.push(...servicesResult.services);
                this.tickets.push(...servicesResult.tickets);

                this.state = 'loaded';
            } else {
                console.error('Could not get services from the response payload.');
                this.state = 'error';
            }
        } catch (error) {
            console.error('Unexpected error has occurred when loading services.', error);
            this.state = 'error';
        }
    }

    public getPrice = async (
        count: number,
        duration: number,
        serviceId: number,
        clientId: string,
    ) => {
        try {
            const {
                value: price,
            } = await postFunctionApi<{ value: number }>(
                `/clients/${clientId}/season-ticket/get-price`,
                {
                    count,
                    duration,
                    serviceId,
                },
            );
            return price;
        } catch (error) {
            console.log('error', error);
            
            return 99999;
        }
    };
}

