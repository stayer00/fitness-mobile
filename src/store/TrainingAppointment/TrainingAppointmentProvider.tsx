import React, { createContext } from 'react';

import { useLocalStore } from 'mobx-react';

import { TrainingAppointmentStore } from './TrainingAppointmentStore';

export const TrainingAppointmentContext = createContext<TrainingAppointmentStore | null>(null);

export const TrainingAppointmentProvider = ({ children, id }: { children: React.ReactNode, id: string }) => {
    const store = useLocalStore(() => new TrainingAppointmentStore(id));
    return <TrainingAppointmentContext.Provider value={store}>{children}</TrainingAppointmentContext.Provider>;
};
