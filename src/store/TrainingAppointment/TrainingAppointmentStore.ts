import { observable, makeObservable } from 'mobx';

import { ClientTraining, TrainingAppointment } from '@interfaces/interfaces';

import { deleteFunctionApi, fetchFunctionApi, putFunctionApi } from '../../helpers/fetchFunction';

export class TrainingAppointmentStore {
    public trainingAppointments: TrainingAppointment[] = [];

    public state: 'loading' | 'loaded' | 'error' = 'loading';

    constructor(private clientId: string) {
        makeObservable(this, {
            trainingAppointments: observable,
            state: observable,
        });

        if (process.env.NODE_ENV !== 'test') {
            this.loadClientTrainingAppointments(clientId, new Date().getMonth());
        }
    }

    public async loadClientTrainingAppointments(id: string, month: number) {
        this.state = 'loading';

        try {
            const getClient = (clients: ClientTraining[]) => (
                clients?.find(j => j.clientId === id)
            );

            const trainingAppointments = await fetchFunctionApi<TrainingAppointment[]>(
                `/training-appointments/client/${id}/${month + 1}`,
            );
            
            if (trainingAppointments && Array.isArray(trainingAppointments)) {
                this.trainingAppointments = [];

                const handledTrainingAppointments: Array<TrainingAppointment> = [];

                trainingAppointments.forEach((i) => {
                    handledTrainingAppointments.push({
                        ...i,
                        currentClient: Boolean(getClient(i.clients)),
                        isVisited: getClient(i.clients)?.isVisited,
                        date: i.date.includes('Z') ? i.date : `${i.date}Z`,
                    });
                });

                this.trainingAppointments.push(...handledTrainingAppointments);
                this.state = 'loaded';
            } else {
                console.error('Could not get trainingAppointments from the response payload.');
                this.state = 'error';
            }
        } catch (error) {
            console.error('Unexpected error has occurred when loading clients.', error);
            this.state = 'error';
        }
    }

    public setClientToTrainingAppointment = async (idTrainingAppointment: number, idSt: number) => {
        try {
            this.state = 'loading';

            const answer = await putFunctionApi<ClientTraining>(`/training-appointments/${idTrainingAppointment}/clients`,
                {
                    seasonTicketId: idSt,
                });
            if (answer.id) {
                const trIdx = this.trainingAppointments.findIndex(i => i.id === idTrainingAppointment);
                if (trIdx !== -1) {
                    const tr = this.trainingAppointments[trIdx];
                    tr.clients.push(...
                    [{
                        id: answer.id,
                        seasonTicketId: answer.seasonTicketId,
                        clientId: answer.clientId,
                        userName: answer.userName,
                    }]);
                    tr.currentClient = !tr.currentClient;
                    
                    this.trainingAppointments.splice(
                        trIdx,
                        1,
                        tr,
                    );
                    
                    this.state = 'loaded';
                    return true;
                }
            }
            this.state = 'loaded';
        } catch (error) {
            console.error('Unexpected error has occurred when setting client to training-appointment.', error);
            return false;
        }
        this.state = 'loaded';
    };

    public unsetClientToTrainingAppointment = async (idTrainingAppointment: number, idSt: number) => {
        try {
            this.state = 'loading';

            const answer = await deleteFunctionApi<any>(`/training-appointments/${idTrainingAppointment}/clients/${idSt}`);
            if (answer.status === 200) {
                const clientIdx = this.trainingAppointments.find(i => i.id === idTrainingAppointment)?.clients.findIndex(i => i.seasonTicketId === idSt);
                
                if (clientIdx !== undefined) {
                    const trIdx = this.trainingAppointments.findIndex(i => i.id === idTrainingAppointment);

                    if (trIdx !== -1) {
                        const tr = this.trainingAppointments[trIdx];
                        tr.clients.splice(clientIdx, 1);

                        this.trainingAppointments.splice(
                            trIdx,
                            1,
                            {
                                ...tr,
                                currentClient: undefined,
                            },
                        );

                        this.trainingAppointments = [ ...this.trainingAppointments];
                    }
                }
                this.state = 'loaded';
                return true;
            }
        } catch (error) {
            console.error('Unexpected error has occurred when deleting client from training-appointment.', error);
            return false;
        }
        this.state = 'loaded';
    };
}

