import React, { createContext } from 'react';

import { useLocalStore } from 'mobx-react';

import { AuthStore } from './AuthStore';

export const AuthContext = createContext<AuthStore | null>(null);

export const AuthProvider = ({ children }: { children: React.ReactNode }) => {
    const store = useLocalStore(() => new AuthStore());
    return <AuthContext.Provider value={store}>{children}</AuthContext.Provider>;
};
