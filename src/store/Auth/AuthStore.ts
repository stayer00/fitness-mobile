import { observable, makeObservable } from 'mobx';

import { postFunctionAuth } from '../../helpers/fetchFunction';
import { AuthInterface, ILogin, IRegisterAnswer, IRegisterUser } from './AuthIterface';

export enum TypeUser {
    Administator,
    Coach,
    Client,
    Guest,
}

export type TypesUser = TypeUser.Administator | TypeUser.Coach | TypeUser.Client | TypeUser.Guest;

export enum AuthType {
    Authorized = 'authorized',
    Unauthorized = 'unauthorized',
}
export class AuthStore {
    public auth: AuthType = AuthType.Unauthorized;

    public typeUser: TypesUser = TypeUser.Guest;

    public userId: string = '';

    public balance: number = 0;

    public pushToken: string = '';

    constructor() {
        makeObservable(this, {
            auth: observable,
            typeUser: observable,
            balance: observable,
        });
        this.getAuth();
    }

    private getAuth = async () => (
        Promise.all([
            this.isAuthenticated(),
            this.isAdmin(),
        ])
    );

    private isAuthenticated = async () => {
        try {
            const isAuth = await postFunctionAuth<AuthInterface>('/isAuthenticated', {});
            
            if (isAuth.isAuth) {
                this.auth = AuthType.Authorized;
            } else {
                this.auth = AuthType.Unauthorized;
            }
            
            this.userId = isAuth.userId;
        } catch (error) {
            console.error('isAuthenticated', error);
        }
    };

    private isAdmin = async () => {
        try {
            const isAdmin = await postFunctionAuth<any>('/is-admin', {});
            
            if (isAdmin) {
                this.typeUser = TypeUser.Administator;
            }
        } catch (error) {
            if ((error as any).httpStatus === 401) {
                if (this.auth === AuthType.Authorized) {
                    this.typeUser = TypeUser.Client;
                } else {
                    this.typeUser = TypeUser.Guest;
                }
            }
        }
    };

    public register = async (registerUser: IRegisterUser, needSign: boolean) => {
        try {
            const register = { ...registerUser, typeUser: 2, needSign };
            const answer = await postFunctionAuth<IRegisterAnswer>(
                '/register',
                { ...register, pushTokens: this.pushToken },
            );
            
            console.log('answer', answer);
            
            if (answer.error) {
                return false;
            } else {
                return answer.data;
            }
        } catch (error) {
            console.log('error', error);
            
            console.error('Error while register', error);
        }
        return false;
    };

    private validateEmail = (email: string) => {
        if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
            throw new Error('Login is not email');
        }
    };

    private validatePassword = (password: string) => {
        if (password.length < 6) {
            throw new Error('Password length must be more than 6');
        }
        
        if (!/\d/.test(password)) {
            throw new Error('Password must have numbers');
        }

        if (!/[A-Z, a-z]/.test(password)) {
            throw new Error('Password must have characters');
        }
    };

    public login = async (loginObj: ILogin) => {
        try {
            // this.validateEmail(loginObj.email);
            // this.validatePassword(loginObj.password);
    
            const answer = await postFunctionAuth<any>(
                '/login',
                { ...loginObj, pushTokens: this.pushToken },
            );
            console.log('answer', answer);
            
            if (answer.error) {
                return false;
            } else {
                await this.isAuthenticated();
    
                return true;
            }
        } catch (error) {
            console.log('error', error);
        }
    };

    public logout = async () => {
        const answer = await postFunctionAuth<any>('/logoff', {});
        
        if (answer.error) {
            return false;
        } else {
            this.auth = AuthType.Unauthorized;
            document.location.reload();
            return true;
        }
    };

    public setPushToken = (token: string) => {
        this.pushToken = token;
    };
}

// decorate(AuthStore, {
//     auth: observable,
//     typeUser: observable,
//     balance: observable,
// });
