import { Client } from 'interfaces/interfaces';

export interface AuthInterface {
    message: string,
    isAuth: boolean,
    userId: string,
    balance: number,
}

export interface IRegisterUser {
    name: string,
    surname: string,
    password: string,
    passwordConfirm: string,
    numberPhone: string,
    email: string,
    gender: string,
    birthday: string,
}

export interface IRegisterAnswer {
    data: Client,
    error?: any,
}

export interface ILoginAnswer {
    message: string,
    error?: any,
    type?: any,
}

export interface ILogin {
    email: string,
    password: string,
}