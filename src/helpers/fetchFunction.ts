// import CookieManager from '@react-native-cookies/cookies';

const API_URL = 'https://f362-212-124-7-177.ngrok.io/api';
const AUTH_URL = 'https://f362-212-124-7-177.ngrok.io/api/account';
// const API_URL = 'https://192.168.1.204/api';
// const AUTH_URL = 'https://192.168.1.204/api/account';

class HttpError extends Error {
    httpStatus: number;

    constructor(message: string, httpStatus: number) {
        super(message);
        this.httpStatus = httpStatus;
    }
}

const handleResponse = async <T>(response: Response): Promise<T> => {
    if (response.ok) {
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
            try {
                const resp = await response.json();
                return resp || [];
            } catch (error) {
                throw new Error(error);
            }
        }
        return (response as unknown) as T;
    }
    throw new HttpError(`Response: [${response.status}] ${response.statusText}`, response.status);
};

export const fetchFunctionAuth = async <T>(urlParams: string): Promise<T> => {
    const res = await fetch(`${AUTH_URL}${urlParams}`);
    return handleResponse<T>(res);
};

export const postFunctionAuth = async <T>(urlParams: string, body: any): Promise<T> => new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', `${AUTH_URL}${urlParams}`, true);

    //Передаёт правильный заголовок в запросе
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.setRequestHeader('access-control-expose-headers', '*');
    xhr.setRequestHeader('Access-Control-Allow-Credentials', 'true');
    xhr.withCredentials = true;

    xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
        if (xhr.readyState == XMLHttpRequest.DONE) {
            
            if (xhr.status >= 200 && xhr.status < 300) {
                // if (urlParams === '/login') {
                //     console.log('xhr.status', xhr.status);
                //     const cookie = xhr.getResponseHeader('Set-Cookie');
                //     console.log({
                //         cookie,
                //     });

                //     if (cookie) {
                //         CookieManager.setFromResponse(
                //             'http://example.com',
                //             'cookie',
                //         ).then((success: any) => {
                //             console.log('CookieManager.setFromResponse =>', success);
                //         });

                //         CookieManager.get('http://example.com').then(
                //             (cookies: any) => {
                //                 console.log('CookieManager.get =>', cookies);
                //             },
                //         );
                //     }
                      
                // }

                resolve(JSON.parse(xhr.response));
            } else {
                reject(xhr.response);
            }
        }
    };
    xhr.send(JSON.stringify(body));
});

export const fetchFunctionApi = async <T>(urlParams: string | URL, requestInit?: RequestInit): Promise<T> => {
    let res;
    if (urlParams instanceof URL) {
        res = await fetch(urlParams.toString(), {
            credentials: 'include',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'access-control-expose-headers': '*',
            },
        });
    } else {
        res = await fetch(`${API_URL}${urlParams}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache': 'no-cache',
            },
            credentials: 'include',
            // mode: 'no-cors'
        });
    }

    return handleResponse<T>(res);
};

export const postFunctionApi = async <T>(urlParams: string, body: any): Promise<T> => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', `${API_URL}${urlParams}`, true);
    
        //Передаёт правильный заголовок в запросе
        xhr.setRequestHeader('Content-Type', 'application/json');
        // xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        // xhr.setRequestHeader('access-control-expose-headers', '*');
        // xhr.setRequestHeader('Access-Control-Allow-Credentials', 'true');
        xhr.withCredentials = true;
    
        xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
            if (xhr.readyState == XMLHttpRequest.DONE) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve(xhr.response ? JSON.parse(xhr.response) : xhr.response);
                } else {
                    reject(xhr.status);
                }
            }
        };
        xhr.send(JSON.stringify(body));
    });
    // const res = await fetch(`${API_URL}${urlParams}`, {
    //     method: 'POST',
    //     body: JSON.stringify(body),
    //     credentials: 'include',
    //     headers: {
    //         'Content-Type': 'application/json',
    //     },
    // });
    // return handleResponse<T>(res);
};

export const putFunctionApi = async <T>(urlParams: string | URL, body: any): Promise<T> => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('PUT', `${API_URL}${urlParams}`, true);
    
        //Передаёт правильный заголовок в запросе
        xhr.setRequestHeader('Content-Type', 'application/json');
        // xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        // xhr.setRequestHeader('access-control-expose-headers', '*');
        // xhr.setRequestHeader('Access-Control-Allow-Credentials', 'true');
        xhr.withCredentials = true;
    
        xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
            if (xhr.readyState == XMLHttpRequest.DONE) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve(JSON.parse(xhr.response));
                } else {                    
                    reject(xhr.response);
                }
            }
        };
        xhr.send(JSON.stringify(body));
    });
    const res = await fetch(`${API_URL}${urlParams}`, {
        method: 'PUT',
        body: JSON.stringify(body),
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    return handleResponse<T>(res);
};

export const deleteFunctionApi = async <T>(urlParams: string | URL): Promise<T> => {
    const res = await fetch(`${API_URL}${urlParams}`, {
        method: 'DELETE',
        credentials: 'include',
    });
    return handleResponse<T>(res);
};
